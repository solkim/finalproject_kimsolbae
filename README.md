# finalproject_kimsolbae

## 🦁 MutsaSNS


- 멋쟁이 사자처럼 백엔드 스쿨 2기 개인 종합 프로젝트
  - SNS에 회원가입, 로그인하여 CRUD 할 수 있는 웹 페이지 구현
- EndPoint http://54.180.86.39:8082/swagger-ui/

## 개요


- 에디터 : Intellij
- 개발 툴 : SpringBoot 2.7.5
- 자바 : JAVA 11
- 서버 : AWS EC2
- 배포 : Docker, Gitlab
- DB : MariaDB
- 필수 라이브러리 : SpringBoot Web, Spring Security, MariaDB, Spring Data JPA, Lombok

## ERD

![image](https://user-images.githubusercontent.com/103480627/211474766-b9170d99-8774-489d-a9cf-dd58ef0eaa4a.png)

## 🎨 진행과정

- [x] gitlab 배포파일 및 ec2 크론탭 설정
- [x] swagger 문서화 설정
- [x] 회원가입과 로그인
- [x] 게시글 CRUD
- [x] 댓글 기능 구현
- [x] 좋아요 기능 구현
- [x] 테스트 코드 작성하기
- [x] 마이페이지 기능 구현
- [x] 알람 기능 구현
- [x] admin 권한 (Role 역할) 구현
- [ ] 화면ui 설정


## 🎯 ENDPOINT


|API 종류|HTTP|URI|API 설명|
|:-----:|:------------------:|:-----------------------------:|:-----------------------------:|
| `hello` | GET | /api/v1/hello | testAPI return String |
| `users` | POST | /api/v1/users/join | 회원가입 |
| `users` | POST | /api/v1/users/login | 로그인 |
| `users` | POST | /api/v1/users/{id}/role/change | 사용자권한변경 |
| `posts` | POST | /api/v1/posts | 글작성 |
| `posts` | PUT | /api/v1/posts/{id} | 글수정 |
| `posts` | DELETE | /api/v1/posts/{id} | 글삭제 |
| `posts` | GET | /api/v1/posts/{id} | 글조회 |
| `posts` | GET | /api/v1/posts | 글전체 |
| `comment` | POST | /api/v1/posts/{id}/comment | 댓글작성 |
| `comment` | PUT | /api/v1/posts/{postid}/comment/{id} | 댓글수정 |
| `comment` | DELETE | /api/v1/posts/{postid}/comment/{id} | 댓글삭제 |
| `comment` | GET | /api/v1/posts/{id}/comment | 댓글조회 |
| `likes` | POST | /api/v1/posts/{id}/likes | 좋아요+취소 |
| `likes` | GET | /api/v1/posts/{id}/likes | 좋아요조회 |
| `my` | GET | /api/v1/posts/my | 마이피드조회 |
| `alarm` | GET | /api/v1/users/alarm | 알림조회 |


## 제출 기록

1차 제출 22.12.27일
   - 통일성이 없다.
    - Controller에서 Client로 반환하는 과정에서 통일성 없어서 로직을 이해하는데 자연스럽지가 않다. </br>
      - Service에서 리턴된 값을 Controller에서 받아 바로 리턴하는 형식으로 통일

  - Like기능, Comment기능을 추가해봐야 겠다.
    - 아직 배우지 않은 기능이지만, 충분히 도전 할 수 있을것 같다.
  - Service Test를 적용시켜야한다...
    - Test는 너무 익숙하지 않아서 어려운것 같다. 그래도 계속 도전을 해보자.
    - Service Test는 Spring 사용 없이 하는것을 연습해 보자.
    - Test를 좀 더 꼼꼼히 작성하도록 노력해보자.

## 특이사항

### 1. CICD 구축하는것이 익숙하지 않아서 어려웠다.
  - Gitlab의 pipeline 구축부터, Crontab을 하는 과정이 쉬운것 같았지만 맘대로 되지 않았다.

### 2. Role 권한부여
  - Spring Security에 대한 이해도가 없어서 Role 권한을 로그인시 어떻게 검증하는지 헷갈렸다.
  - 프로젝트를 진행하며 Spring Security에대한 이해도를 높일 수 있었다.

### 3. Security Chain Filter Exception 핸들링
  - JWT를 검증하는 과정에서 에러가 발생하게되면 @RestControllerAdvice를통해서 Exception을 처리하는게 되지 않았다.
  - Internal Server Error가 발생하여 개발자만 Error를 확인할수있어서 불편함이 많다.
  - Filter Chain은 DispatcherServlet(Front Controller) 앞에서 적용된다.
    - 그래서 Security Chain Filter Exception은 Servlet에 가기전에 핸들링 해야했다.
    - `ExceptionHandlerFilter`를 만들어 토큰을 검증하는 필터앞에 예외를 처리하게 했다.
    - 인증예외처리(AuthenticationException), 인가예외처리(AccessDeniedException)를 처리하기위해
       `AuthenticationEntryPoint`와 `AccessDeniedHandler`를 구현했다.

### 4. Test코드 작성
  -  Test코드를 작성하는것이 너무 어려웠다.
     특히 TDD로 개발을 하려고 하면, 진도를 나가는게 눈에띄게 늦어졌다.
     그래서 TDD로 개발을 하다가 막히면 먼저 비지니스 로직을 구현하고 TestCode로 로직을 검증하였다.

     Test Code를 만드는것을 신경써서인지 이제는 조금 눈에 들어오는것 같다.
     꼼꼼한 Test Code를 작성하고 있는지는 모르겠지만, 적어도 의도한 로직을 검증하는 것과 에러를 테스트하는것은 잘 하는것 같다.

### 5. Soft Delete
  -  물리적 삭제와 논리적 삭제를 알게되었는데, 데이터를 관리상 안전한 편에 속한다고 한다.

     그런데 DB에는 삭제하지 않는 개념이다보니, DB의 용량이 커질 수 있다. (지금 프로젝트는 상관없다.)
     또, Soft delete는 조회를 할때 deleteAt이 null인것만 조회하게 검색조건을 설정해주어야 한다.
     여기서 로직을 구현하는데 시간이 조금 걸렸다.

     [Soft Delete가 안된다❓(@Where 어노테이션 주의사항) ](https://velog.io/@readnthink/%EC%A2%8B%EC%95%84%EC%9A%94-%EA%B8%B0%EB%8A%A5-%EC%A4%91%EB%B3%B5%EC%B2%98%EB%A6%AC-%EC%8B%9C-Soft-Delete-%EC%98%A4%EB%A5%98...-Hard-Delete%EA%B0%80-%EB%90%98%EA%B3%A0%EC%9E%88%EB%8B%A4)

