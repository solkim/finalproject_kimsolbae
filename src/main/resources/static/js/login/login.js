document.getElementById("login").addEventListener('click',login);

async function login() {
    let response = await fetch("/api/v1/users/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            username: document.getElementById("username").value,
            password: document.getElementById("password").value
        })
    })
    if(response.ok){
        let json = await response.json();
        localStorage.setItem("Authorization","Bearer " + json.result.accessToken);
        console.log("Token = "+ localStorage.getItem("Authentication"));
        console.log(json.result)
        location.href="/posts/list"

    }else {
        let json = await response.text();
        alert(json.result)
        console.log(json.result)
    }
    ;
}