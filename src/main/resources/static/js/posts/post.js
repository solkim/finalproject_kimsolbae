document.getElementById("post_btn").addEventListener('click', postAdd);

async function postAdd() {
    let response = await fetch("/api/v1/posts", {
        method:"POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem("Authorization"),
            "Authentication": localStorage.getItem("Authorization")
        },
        body: JSON.stringify({
            content: document.getElementById("body").value,
            title: document.getElementById("title").value
        })
    })
    if (response.ok) {
        let json = await response.json();
        console.log(json)
        location.href="/posts/list"
    } else {
        let json = await response.json();
        // if (json.getItem(""))
        console.log(json.contains("UNAUTHORIZED"))
    }
}