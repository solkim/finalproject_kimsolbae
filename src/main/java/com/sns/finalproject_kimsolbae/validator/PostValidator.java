package com.sns.finalproject_kimsolbae.validator;


import com.sns.finalproject_kimsolbae.domain.dto.post.PostRequest;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.thymeleaf.util.StringUtils;

@Component
public class PostValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return PostRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        PostRequest b = (PostRequest) obj;
        if (StringUtils.isEmpty(b.getBody())) {
            errors.rejectValue("body", "key", "내용을 입력하세요.");
        }
    }
    public void validatePost(Object obj, Errors errors) {
        Post b = (Post) obj;
        if (StringUtils.isEmpty(b.getBody())) {
            errors.rejectValue("body", "key", "내용을 입력하세요.");
        }
        if (StringUtils.isEmpty(b.getTitle())) {
            errors.rejectValue("title", "key", "제목을 입력하세요.");
        }
    }
}
