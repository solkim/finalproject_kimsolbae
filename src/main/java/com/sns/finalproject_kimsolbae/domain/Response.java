package com.sns.finalproject_kimsolbae.domain;

import com.sns.finalproject_kimsolbae.domain.dto.ErrorResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;

@AllArgsConstructor
@Getter
public class Response<T> {
    private String resultCode;
    private T result;

    //에러 리턴
    public static Response<ErrorResponse> error(ErrorResponse errorResponse) {
        return new Response("ERROR", errorResponse);
    }
    // 성공 리턴
    public static <T> Response<T> success(T result) {
        return new Response("SUCCESS", result);
    }
}
