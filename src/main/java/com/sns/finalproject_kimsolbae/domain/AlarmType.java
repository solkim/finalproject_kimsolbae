package com.sns.finalproject_kimsolbae.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AlarmType {
    NEW_COMMENT_ON_POST("new Comment!"),
    NEW_LIKE_ON_POST("new like!")
    ;

    String text;
}
