package com.sns.finalproject_kimsolbae.domain.dto.user;

import com.sns.finalproject_kimsolbae.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserLoginResponse {
    private Long id;
    private String username;

    public static UserLoginResponse of(User user) {
        return new UserLoginResponse(user.getId(), user.getUsername());
    }
}
