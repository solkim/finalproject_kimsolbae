package com.sns.finalproject_kimsolbae.domain.dto.post;

import com.sns.finalproject_kimsolbae.domain.entity.BaseEntity;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class PostOneResponse{
    private Long id;
    private String title;
    private String body;
    private String username;

    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;

    public static PostOneResponse fromEntity(Post post) {
        return PostOneResponse.builder()
                .id(post.getId())
                .title(post.getTitle())
                .body(post.getBody())
                .username(post.getUser().getUsername())
                .createdAt(post.getCreatedAt())
                .lastModifiedAt(post.getLastModifiedAt())
                .build();
    }
}
