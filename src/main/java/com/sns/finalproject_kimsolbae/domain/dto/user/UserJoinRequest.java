package com.sns.finalproject_kimsolbae.domain.dto.user;

import com.sns.finalproject_kimsolbae.domain.UserRole;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinRequest {
    private String username;
    private String password;

    public static User toEntity(UserJoinRequest dto, BCryptPasswordEncoder encoder) {
        UserRole userRole = UserRole.ROLE_USER;
        //초기 ADMIN회원 1명 (id가 sol이라면)
        if(dto.username.equals("sol")) userRole = UserRole.ROLE_ADMIN;

        User user = User.builder()
                .username(dto.username)
                .password(encoder.encode(dto.password))
                .role(userRole)
                .build();
        return user;
    }
}
