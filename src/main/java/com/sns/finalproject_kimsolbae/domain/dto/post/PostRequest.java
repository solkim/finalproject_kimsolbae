package com.sns.finalproject_kimsolbae.domain.dto.post;

import com.sns.finalproject_kimsolbae.domain.entity.Post;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PostRequest {
    @NotNull
    @Size(min=2, max=30)
    private String title;
    @NotNull
    @Size(min=2,max=300)
    private String body;

    public Post toEntity(User user) {
        return Post.builder()
                .title(this.title)
                .body(this.body)
                .user(user)
                .build();
    }
}
