package com.sns.finalproject_kimsolbae.domain.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CommentDeleteResponse {
    private String message;
    private Long id;

    public static CommentDeleteResponse of(Long postId) {
        return new CommentDeleteResponse("댓글 삭제 완료", postId);
    }
}
