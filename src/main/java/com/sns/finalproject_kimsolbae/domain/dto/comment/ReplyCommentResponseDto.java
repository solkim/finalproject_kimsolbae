package com.sns.finalproject_kimsolbae.domain.dto.comment;

import com.sns.finalproject_kimsolbae.domain.entity.Comment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ReplyCommentResponseDto {
    private Long id;
    private String comment;
    private String userName;
    private Long postId;
    private Long parentId;
    private LocalDateTime createdAt;

    public ReplyCommentResponseDto(Comment comment, String userName, Long postId, Long parentId) {
        this.id = comment.getId();
        this.comment = comment.getComment();
        this.userName = userName;
        this.postId = postId;
        this.parentId = parentId;
        this.createdAt = LocalDateTime.now();
    }
}
