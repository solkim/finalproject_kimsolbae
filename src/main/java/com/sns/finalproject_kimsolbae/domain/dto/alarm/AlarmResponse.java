package com.sns.finalproject_kimsolbae.domain.dto.alarm;

import com.sns.finalproject_kimsolbae.domain.AlarmType;
import com.sns.finalproject_kimsolbae.domain.entity.Alarm;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class AlarmResponse {
    private Long id;
    private AlarmType alarmType;
    private Long fromUserId;
    private Long targetId;
    private String text;
    private LocalDateTime createdAt;

    public static AlarmResponse fromEntity(Alarm alarm) {
        return AlarmResponse.builder()
                .id(alarm.getId())
                .alarmType(AlarmType.NEW_LIKE_ON_POST)
                .fromUserId(alarm.getFromUserId())
                .targetId(alarm.getTargetId())
                .text(AlarmType.NEW_LIKE_ON_POST.getText())
                .createdAt(alarm.getCreatedAt())
                .build();
    }
}
