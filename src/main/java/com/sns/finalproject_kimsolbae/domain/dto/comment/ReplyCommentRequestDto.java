package com.sns.finalproject_kimsolbae.domain.dto.comment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ReplyCommentRequestDto {
    private String replyComment;
}
