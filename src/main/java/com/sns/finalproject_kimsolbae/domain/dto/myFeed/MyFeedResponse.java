package com.sns.finalproject_kimsolbae.domain.dto.myFeed;

import com.sns.finalproject_kimsolbae.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class MyFeedResponse{
    private Long id;
    private String title;
    private String body;
    private String username;

    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;

    public static MyFeedResponse fromEntity(Post post) {
        return MyFeedResponse.builder()
                .id(post.getId())
                .title(post.getTitle())
                .body(post.getBody())
                .username(post.getUser().getUsername())
                .createdAt(post.getCreatedAt())
                .lastModifiedAt(post.getLastModifiedAt())
                .build();
    }
}
