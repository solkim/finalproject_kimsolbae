package com.sns.finalproject_kimsolbae.domain.dto;

import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ErrorResponse {
    private String errorCode;
    private String message;
}
