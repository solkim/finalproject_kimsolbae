package com.sns.finalproject_kimsolbae.domain.dto.post;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.bind.annotation.PutMapping;

import java.security.PublicKey;

@Getter
@AllArgsConstructor
public class PostResponse {
    private String message;
    private Long postId;

    public static PostResponse of(String message, Long postId) {
        return new PostResponse(message, postId);
    }
}
