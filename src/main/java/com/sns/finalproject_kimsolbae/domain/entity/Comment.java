package com.sns.finalproject_kimsolbae.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Comment extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id") //외래키 : DB에 해당 명으로 컬럼설정
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id") //DB에 해당 명으로 컬럼설정
    private Post post;


    private Integer depth;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_comment_id")
    private Comment parentComment;

    @OneToMany(mappedBy = "parentComment", orphanRemoval = true)
    private List<Comment> subComment = new ArrayList<>();

    public Comment(String comment, User user, Post post, Comment parent) {
        this.comment = comment;
        this.user = user;
        this.post = post;
        this.parentComment = parent;
    }

    public static Comment createReplyComment(String comment, User user, Post post, Comment parent) {
        return new Comment(comment, user, post, parent);
    }
}
