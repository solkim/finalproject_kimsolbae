package com.sns.finalproject_kimsolbae.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;

@AllArgsConstructor
@Getter
@RedisHash(value = "redisToken", timeToLive = 30)
public class RedisToken {
    @Id
    private Long id;
    private String accessToken;
    private String refreshToken;
}
