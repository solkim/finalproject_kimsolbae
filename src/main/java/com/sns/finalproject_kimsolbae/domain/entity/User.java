package com.sns.finalproject_kimsolbae.domain.entity;

import com.sns.finalproject_kimsolbae.domain.UserRole;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinResponse;
import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
@Table(name = "Users")
public class User extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;

    @OneToMany(mappedBy = "user") //post의 user필드와 연결
    @Builder.Default
    private List<Post> posts = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    @Builder.Default
    private List<Comment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    @Builder.Default
    private List<Like> likes = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    @Builder.Default
    private List<Alarm> alarms = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private UserRole role;

    public UserJoinResponse fromEntity() {
        return new UserJoinResponse(this.id, this.username);
    }
}
