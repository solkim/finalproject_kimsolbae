package com.sns.finalproject_kimsolbae.domain.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass  //BaseEntity를 상속한 entity들은 BaseEntity의 멤버변수를 모두 컬럼으로 인식
@EntityListeners(AuditingEntityListener.class) //entity를 DB에 적용 전후로 콜백(auditing 정보를 주입하는 클래스)
@ToString
public class BaseEntity {

    @CreatedDate
    @Column(updatable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column
    private LocalDateTime lastModifiedAt;


    private LocalDateTime deletedAt;

    // 삭제
    public void deleteSoftly(LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    // 확인
    public boolean isSoftDeleted() {
        return null != deletedAt;
    }
    // 삭제 취소
    public void undoDeletion(){
        this.deletedAt = null;
    }
}