package com.sns.finalproject_kimsolbae.domain.entity;

import com.sns.finalproject_kimsolbae.domain.AlarmType;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class Alarm extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user; //post의 주인
    private AlarmType alarmType; //Like, Comment
    private Long fromUserId; //누가 등록했는지
    private Long targetId; // 어떤 post에 남겼는지
    private String text;

    public static Alarm toEntity(User user, Post post, AlarmType alarmType) {
        Alarm alarm = Alarm.builder()
                .user(post.getUser())
                .alarmType(alarmType)
                .fromUserId(user.getId())
                .targetId(post.getId())
                .text(alarmType.getText())
                .build();
        return alarm;
    }

}
