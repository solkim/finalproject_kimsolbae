package com.sns.finalproject_kimsolbae.domain;

public enum UserRole {
    ROLE_USER, ROLE_ADMIN
}
