//package com.sns.finalproject_kimsolbae.config;
//
//import com.sns.finalproject_kimsolbae.config.auth.JwtAuthenticationFilter;
//import com.sns.finalproject_kimsolbae.config.redis.RedisDao;
//import com.sns.finalproject_kimsolbae.config.securityErrorHandling.ExceptionHandlerFilter;
//import com.sns.finalproject_kimsolbae.service.UserService;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//import org.springframework.security.web.context.SecurityContextPersistenceFilter;
//import org.springframework.security.web.session.DisableEncodeUrlFilter;
//
//@RequiredArgsConstructor
//@EnableWebSecurity(debug = true)
//public class securityCon extends WebSecurityConfigurerAdapter {
//
//    private final UserService userService;
//    private final RedisDao redisDao;
//    @Value("${jwt.secret}")
//    private String secretKey;
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable();
//        http.cors().and();
//        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                .formLogin().disable()
//                .httpBasic().disable()
////                .addFilterBefore(new MyFilter(), DisableEncodeUrlFilter.class) //@EnableWebSecurity(debug = true)로 하고 필터 제일 앞에 넣어주었다.
//                .addFilter(new JwtAuthenticationFilter(authenticationManager(), userService)) //로그인 form 대신 만들어진것이다.
//                .authorizeRequests()
//                .antMatchers("/api/v1/users/join", "/api/v1/users/login", "/users/**", "/main", "/login", "/join" ).permitAll() // join, login은 언제나 가능
//                .antMatchers(HttpMethod.POST, "/api/v1/posts/**", "/posts/write", "/posts/modify").authenticated()
//                .antMatchers(HttpMethod.POST, "/api/v1/users/{id}/role/change", "/users/{id}/role/change").access("hasRole('ROLE_ADMIN')")
//                .antMatchers(HttpMethod.PUT, "/api/**").authenticated()
//                .antMatchers(HttpMethod.DELETE, "/api/**").authenticated()
//                .antMatchers(HttpMethod.GET, "/api/v1/posts/my", "/api/v1/alarms", "/posts/list").authenticated()
//                .anyRequest().permitAll()
//                .and()
////                .formLogin()
////                .loginPage("/users/login").permitAll() //인증이 되지 않으면 login페이지로 이동
////                .and()
//                .logout().permitAll()
//                .and()
//                .exceptionHandling()// 예외처리기능 작동
//                //아래 두개의 클래스가 없어도 예외발생이 된다?
////                .authenticationEntryPoint(new CustomAuthenticationEntryPointHandler()) // 인증처리 실패시 처리
////                .accessDeniedHandler(new CustomAccessDeniedHandler())// 인가처리 실패시 처리
//                .and()
//                .sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS) // STATELESS = jwt사용하는 경우 씀 : 매번 토큰을 사용하는 개념?
//                .and()
//                .addFilterBefore(new JwtFilter(userService, secretKey, redisDao), UsernamePasswordAuthenticationFilter.class) //UserNamePasswordAuthenticationFilter적용하기 전에 JWTTokenFilter를 적용 하라는 뜻.
//                .addFilterBefore(new ExceptionHandlerFilter(secretKey, userService), JwtFilter.class)
//        ;
//    }
//}
