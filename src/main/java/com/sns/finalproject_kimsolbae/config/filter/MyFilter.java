//package com.sns.finalproject_kimsolbae.config.filter;
//
//import javax.servlet.*;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//
//public class MyFilter implements Filter {
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest req = (HttpServletRequest) request;
//        HttpServletResponse res = (HttpServletResponse) response;
//
//        //토큰을 만들었다고 가정하고 인증해보기
//        if (req.getMethod().equals("POST")) {
//            System.out.println("Post 요청됨");
//            String headerAuth = req.getHeader("Authorization");
//            System.out.println(headerAuth);
//            System.out.println("필터1");
//
//            if (headerAuth.equals("cos")) {
//                chain.doFilter(req,res);
//            }else {
//                response.setContentType("application/json;charset=UTF-8");
//                PrintWriter out = res.getWriter();
//                out.println("인증안됨");
//            }
//        }
//    }
//}
