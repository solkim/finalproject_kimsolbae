//package com.sns.finalproject_kimsolbae.config.filter;
//
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class FilterConfig {
//
//    @Bean
//    public FilterRegistrationBean<MyFilter> filter1() {
//        FilterRegistrationBean<MyFilter> bean = new FilterRegistrationBean<>(new MyFilter());
//        bean.addUrlPatterns("/*");
//        bean.setOrder(0); // 순서가 낮은 필터가 먼저 실행됩니다.
//        return bean;
//    }
//}
