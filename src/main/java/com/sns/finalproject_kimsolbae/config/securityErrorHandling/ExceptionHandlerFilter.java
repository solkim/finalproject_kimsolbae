package com.sns.finalproject_kimsolbae.config.securityErrorHandling;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.config.redis.RedisDao;
import com.sns.finalproject_kimsolbae.domain.Response;
import com.sns.finalproject_kimsolbae.domain.UserRole;
import com.sns.finalproject_kimsolbae.domain.dto.ErrorResponse;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.service.UserService;
import com.sns.finalproject_kimsolbae.util.Subject;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@RequiredArgsConstructor
public class ExceptionHandlerFilter extends OncePerRequestFilter {

    private RedisDao redisDao;
    private final String key;
    private final UserService userService;
    /**
     * 토큰 관련 에러 핸들링
     * JwtTokenFilter 에서 발생하는 에러를 핸들링해준다.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (ExpiredJwtException e) {

            //토큰의 유효기간 만료
            log.error("만료된 토큰입니다");
//            final String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
//            String[] token = authorization.split(" ");
//            log.info(token[1]);
//            log.info(redisDao.getValues("RT:" + token[1]));
//
//            if (redisDao.getValues("RT:"+token[1]) != null) {
//                //userName을 통해 Role 꺼내기
//                User user = userService.getUserByUserName(token[1]);
//                UserRole role = user.getRole();
//
//                //payload(authentication)에 들어갈 subject 만들기
//                Subject atkSubject = Subject.atk(user.getId(), user.getUserName());
//                log.info("subject = {}", atkSubject.getUserName());
//                //문 열어주기
//                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken
//                        (atkSubject, null, List.of(new SimpleGrantedAuthority(role.toString())));
//
//                //Detail을 넣는다.
//                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//                filterChain.doFilter(request,response);
//            }

            setErrorResponse(response, ErrorCode.EXPIRED_TOKEN);

        } catch (JwtException e) {
            //유효하지 않은 토큰
            log.error("유효하지 않은 토큰이 입력되었습니다.");
            setErrorResponse(response, ErrorCode.INVALID_TOKEN);

        }
        catch (IllegalArgumentException e){
            setErrorResponse(response, ErrorCode.INVALID_TOKEN);

        }
        catch (NoSuchElementException e) {
            //사용자 찾을 수 없음
            log.error("사용자를 찾을 수 없습니다.");
            setErrorResponse(response, ErrorCode.USERNAME_NOT_FOUND);
        }catch (ArrayIndexOutOfBoundsException e) {

            log.error("토큰을 추출할 수 없습니다.");
            setErrorResponse(response, ErrorCode.INVALID_TOKEN);

        } catch (NullPointerException e) {

            log.error("토큰이 없습니다.");
            setErrorResponse(response,ErrorCode.INVALID_TOKEN);
        }
    }
    /**
     * Security Chain 에서 발생하는 에러 응답 구성
     */
    public static void setErrorResponse(HttpServletResponse response, ErrorCode errorCode) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(errorCode.getHttpStatus().value());
        ObjectMapper objectMapper = new ObjectMapper();

        ErrorResponse errorResponse = new ErrorResponse(
                errorCode.getHttpStatus().name(), errorCode.getMessage());

        Response<ErrorResponse> error = Response.error(errorResponse);
        String s = objectMapper.writeValueAsString(error);
        log.info("errorCode = {}",s);

        /**
         * 한글 출력을 위해 getWriter() 사용
         */
        response.getWriter().write(s);
    }
}

