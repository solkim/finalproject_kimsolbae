package com.sns.finalproject_kimsolbae.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@Configuration
@EnableJpaAuditing
public class JpaAuditingConfiguration {
}
