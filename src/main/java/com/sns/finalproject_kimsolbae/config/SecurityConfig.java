package com.sns.finalproject_kimsolbae.config;

import com.sns.finalproject_kimsolbae.config.redis.RedisDao;
import com.sns.finalproject_kimsolbae.config.securityErrorHandling.ExceptionHandlerFilter;
import com.sns.finalproject_kimsolbae.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity //(debug = true) // request가 올 떄마다 어떤 filter를 사용하고 있는지 출력을 해준다.
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig{

    private final UserService userService;
    private final RedisDao redisDao;
    //authentication 헤더에 추가하기 위한 필터
//    private final CorsConfig corsConfig;
    @Value("${jwt.secret}")
    private String secretKey;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .httpBasic().disable()
                .csrf().disable()
//                .apply(new MyCustomDsl())
//                .and()
                .cors().and()
//                .addFilter(corsConfig.corsFilter())
                .authorizeRequests()
                .antMatchers("/api/v1/users/join", "/api/v1/users/login", "/users/**", "/main", "/js/**", "/css/**", "/image/**").permitAll() // join, login은 언제나 가능
//                .antMatchers("/hello/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/posts/**", "/posts/write", "/posts/modify").authenticated()
                .antMatchers(HttpMethod.POST, "/api/v1/users/{id}/role/change", "/users/{id}/role/change").access("hasRole('ROLE_ADMIN')")
                .antMatchers(HttpMethod.PUT, "/api/**").authenticated()
                .antMatchers(HttpMethod.DELETE, "/api/**").authenticated()
                .antMatchers(HttpMethod.GET, "/api/v1/posts/my", "/api/v1/alarms").authenticated()
                .and()
                .formLogin()
                .loginPage("/users/login").permitAll()
                .loginProcessingUrl("/auth/loginProc")
                .defaultSuccessUrl("/main")
                .failureUrl("/users/loginForm")
                .and()
                .logout().permitAll()
                .and()
                .exceptionHandling()// 예외처리기능 작동
                //아래 두개의 클래스가 없어도 예외발생이 된다?
//                .authenticationEntryPoint(new CustomAuthenticationEntryPointHandler()) // 인증처리 실패시 처리
//                .accessDeniedHandler(new CustomAccessDeniedHandler())// 인가처리 실패시 처리
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS) // STATELESS = jwt사용하는 경우 씀 : 매번 토큰을 사용하는 개념?
                .and()
                .addFilterBefore(new JwtFilter(userService, secretKey, redisDao), UsernamePasswordAuthenticationFilter.class) //UserNamePasswordAuthenticationFilter적용하기 전에 JWTTokenFilter를 적용 하라는 뜻.
                .addFilterBefore(new ExceptionHandlerFilter(secretKey, userService), JwtFilter.class)
                .build();
    }

//    public class MyCustomDsl extends AbstractHttpConfigurer<MyCustomDsl, HttpSecurity> {
//        @Override
//        public void configure(HttpSecurity http) throws Exception {
//            AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
//            http
//                    .addFilter(corsConfig.corsFilter())
//                    .addFilter(new JwtAuthenticationFilter(authenticationManager, userService))
//                    .addFilter(new JwtAuthorizationFilter(authenticationManager, userService));
//        }
//    }
}