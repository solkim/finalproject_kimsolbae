//package com.sns.finalproject_kimsolbae.config.auth;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.sns.finalproject_kimsolbae.domain.dto.user.UserLoginRequest;
//import com.sns.finalproject_kimsolbae.domain.entity.User;
//import com.sns.finalproject_kimsolbae.service.UserService;
//import com.sns.finalproject_kimsolbae.util.JwtUtil;
//import com.sns.finalproject_kimsolbae.util.Subject;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// *  스프링 시큐리티에서 UsernamePasswordAuthenticationFilter가 있습니다.
// *  /login 요청해서 username, password 전송하면(post)
// *  UsernamePasswordAuthenticationFilter가 동작을 합니다.
// *  그런데 지금은 LoginForm을 사용하지 않을것이니 직접 구현하는것입니다.
// */
//
//@RequiredArgsConstructor
//public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
//
//    private final AuthenticationManager authenticationManager;
//    private final UserService userService;
//    @Value("${jwt.secret}")
//    private String secretKey;
//
//    // /login 요청을 하면 로그인 시도를 위해 실행되는 함수
//
//    @Override
//    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
//        System.out.println("attemptAuthentication : 로그인 시도중");
//
//        /**
//         * authenticationManager로 로그인 시도를 하면 PrincipalDetailsService가 호출되서
//         * principalDetailsService가 호출되고 loadByUsername()이 실행이 됩니다.
//         */
//
//        System.out.println("JwtAuthenticationFilter : 진입");
//
//        // request에 있는 username과 password를 파싱해서 자바 Object로 받기
//        ObjectMapper om = new ObjectMapper();
//        UserLoginRequest loginRequestDto = null;
//        try {
//            loginRequestDto = om.readValue(request.getInputStream(), UserLoginRequest.class);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("JwtAuthenticationFilter : "+loginRequestDto);
//
//        // 유저네임패스워드 토큰 생성
//        UsernamePasswordAuthenticationToken authenticationToken =
//                new UsernamePasswordAuthenticationToken(
//                        loginRequestDto.getUsername(),
//                        loginRequestDto.getPassword());
//
//        System.out.println("JwtAuthenticationFilter : 토큰생성완료");
//
//        // authenticate() 함수가 호출 되면 인증 프로바이더가 유저 디테일 서비스의
//        // loadUserByUsername(토큰의 첫번째 파라메터) 를 호출하고
//        // UserDetails를 리턴받아서 토큰의 두번째 파라메터(credential)과
//        // UserDetails(DB값)의 getPassword()함수로 비교해서 동일하면
//        // Authentication 객체를 만들어서 필터체인으로 리턴해준다.
//
//        // Tip: 인증 프로바이더의 디폴트 서비스는 UserDetailsService 타입
//        // Tip: 인증 프로바이더의 디폴트 암호화 방식은 BCryptPasswordEncoder
//        // 결론은 인증 프로바이더에게 알려줄 필요가 없음.
//        Authentication authentication =
//                authenticationManager.authenticate(authenticationToken);
//
//        PrincipalDetails principalDetailis = (PrincipalDetails) authentication.getPrincipal();
//        System.out.println("Authentication : "+principalDetailis.getUser().getUsername());
//        return authentication;
//    }
//
//    /**
//     *
//     *attemptAuthentication 실행 후 인증ㅇ ㅣ정상적으로 되었으면 successfulAuthentication함수가 실행된다.
//     * JWT토큰을 만들어서 request요청한 사용자에게 JWT토큰을 response해주면 됩니다.
//     *
//     */
//    @Override
//    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
//        PrincipalDetails principalDetailis = (PrincipalDetails) authResult.getPrincipal();
//
//
//        String jwtToken = JwtUtil.createToken(principalDetailis, secretKey, JwtProperties.EXPIRATION_TIME);
//
//        response.addHeader(JwtProperties.HEADER_STRING, JwtProperties.TOKEN_PREFIX+jwtToken);
//    }
//}
