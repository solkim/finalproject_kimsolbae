package com.sns.finalproject_kimsolbae.config.auth;

import org.springframework.beans.factory.annotation.Value;

public interface JwtProperties {

    Long EXPIRATION_TIME = 10 * 60 * 1000L; // (1/1000초)
    String TOKEN_PREFIX = "Bearer ";
    String HEADER_STRING = "Authorization";
}

