package com.sns.finalproject_kimsolbae.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.config.auth.PrincipalDetails;
import com.sns.finalproject_kimsolbae.config.redis.RedisDao;
import com.sns.finalproject_kimsolbae.domain.Response;
import com.sns.finalproject_kimsolbae.domain.UserRole;
import com.sns.finalproject_kimsolbae.domain.dto.ErrorResponse;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.service.UserService;
import com.sns.finalproject_kimsolbae.util.JwtUtil;
import com.sns.finalproject_kimsolbae.util.Subject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

//OncePerRequestFilter = 매번 있는지 확인하기.

@RequiredArgsConstructor
@Slf4j
public class JwtFilter extends OncePerRequestFilter {

    private final UserService userService;
    private final String secretKey;
    private final RedisDao redisDao;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //SecurityConfig에서 Post요청을 전부 막아놓았는데 열여줄지 말지 결정하는 곳이다.

        //헤더에서 authentication을 꺼내서 문을 열어줄지 말지 결정 할 것이다.
        String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        log.info("1. authorization1 : {}", authorization);

        if (authorization == null) {
            //session 인증
            HttpSession session = request.getSession(false);
            if (session == null || session.getAttribute("Authorization") == null) {
                filterChain.doFilter(request,response);
                return;
            } else {
                authorization = session.getAttribute("Authorization").toString();
            }
        }

        //token 안보냈다면 null이라면 권한부여하지 않고 리턴한다.
        if (!authorization.startsWith("Bearer ")) {
            log.error("2. 에러 authorization을 잘못보냈습니다.");
            filterChain.doFilter(request,response);
            return;
        }

        //토큰 꺼내기
        String[] tokens = authorization.split(" ");
        String token = tokens[1];

        //userName 꺼내기
        String userName = JwtUtil.getUserName(token,secretKey);
        log.info("3. username = {}", userName);

        //userName을 통해 Role 꺼내기
        User user = userService.getUserByUsername(userName);
        UserRole role = user.getRole();

        //payload(authentication)에 들어갈 subject 만들기
        Subject atkSubject = Subject.atk(user.getId(), user.getUsername(), role.toString());
        log.info("4. subject = {}", atkSubject.getUsername());

        //Token Expired 되었는지 여부
        if(JwtUtil.isExpired(token,secretKey)){
            log.error("5. 에러 accessToken 이 만료되었습니다.");
            filterChain.doFilter(request,response);
            return;
        }
        log.info("6. ================================문을 열어줍니다.=================================");

        //문 열어주기
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken
                (new PrincipalDetails(user), null, List.of(new SimpleGrantedAuthority(role.toString())));

        //Detail을 넣는다.
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        filterChain.doFilter(request,response);
    }
}
