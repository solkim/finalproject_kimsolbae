package com.sns.finalproject_kimsolbae.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sns.finalproject_kimsolbae.config.redis.RedisDao;
import com.sns.finalproject_kimsolbae.domain.UserRole;
import com.sns.finalproject_kimsolbae.domain.dto.token.TokenResponseDto;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinRequest;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinResponse;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserLoginRequest;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import com.sns.finalproject_kimsolbae.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;
    private final RedisDao redisDao;

    @Value("${jwt.secret}")
    private String key;

    public UserJoinResponse join(UserJoinRequest dto) {
        log.info("Join service \n username : {} password : {}", dto.getUsername(),dto.getPassword());
        // 중복처리
        findByUserName(dto);

        //toEntity에서 id에 sol이라는 단어가 있으면 role에 admin 적용
        User savedUser = userRepository.save(UserJoinRequest.toEntity(dto, encoder));

        //Entity -> dto
        UserJoinResponse userJoinResponse = savedUser.fromEntity();
        return userJoinResponse;
    }


    public TokenResponseDto login(UserLoginRequest dto) throws JsonProcessingException {
        log.info("service username = {}", dto.getUsername());
        //userName 있는지
        User user = getUserByUsername(dto.getUsername());
        //password 일치하는지
        if (!encoder.matches(dto.getPassword(), user.getPassword())) {
            throw new AppException(ErrorCode.INVALID_PASSWORD,ErrorCode.INVALID_PASSWORD.getMessage());
        }

        //token 발생 변경
        TokenResponseDto tokenByLogin = JwtUtil.createTokenByLogin(user, key);
        redisDao.setValues("RT:" + tokenByLogin.getAccessToken(), tokenByLogin.getRefreshToken());
        Long expiration = JwtUtil.getExpiration(tokenByLogin.getAccessToken(),key);
        log.info("\n남은유효기간 = {},     토큰 = {}\n", expiration, tokenByLogin.getAccessToken());

        return tokenByLogin;
    }


    public String logout(TokenResponseDto dto) throws JsonProcessingException {
        //userName 있는지
        if (!JwtUtil.validateToken(dto.getAccessToken(),key)) {
            throw new AppException(ErrorCode.EXPIRED_TOKEN, "로그아웃 에러");
        }
        // 2. Access Token 에서 Username 을 가져온다.
        String userName = JwtUtil.getUserName(dto.getAccessToken(), key);

        // 3. Redis 에서 해당 Username 로 저장된 Refresh Token 이 있는지 여부를 확인 후 있을 경우 삭제합니다.
        if (redisDao.getValues("RT:" + userName) != null) {
            log.info("delete refreshToken = {}", redisDao.getValues("RT:"+userName));
            // Refresh Token 삭제
            redisDao.deleteValues("RT:" + userName);
        }

        // 4. 해당 Access Token 유효시간 가지고 와서 BlackList 로 저장하기
        Long expiration = JwtUtil.getExpiration(dto.getAccessToken(),key);
        redisDao.setValues(dto.getAccessToken(), "logout", Duration.ofMillis(expiration));
        log.info("\n남은유효기간 = {},     토큰 = {}\n", expiration, dto.getAccessToken());


        return "logout에 성공했습니다";
    }

//    public UserLoginResponse reissue(TokenResponseDto tokenRequestDto, String userName) {
//
//        //refreshToken 만료되었는지 확인
//        if (!JwtUtil.isExpired(tokenRequestDto.getRefreshToken(), key)) throw new AppException(ErrorCode.EXPIRED_TOKEN, "refreshToken이 만료되었습니다");
//
//        //만료되지 않았다면 redis에 저장된 토큰 가져온다.
//        String refreshToken = redisDao.getValues("RT" + userName);
//        log.info("refreshToken = {}", refreshToken);
//
//        //새로운 토큰 생성
//        String token = JwtUtil.(userName, key);
//        redisDao.setValues("RT:", userName);
//        return null;
//    }

//    public String logout(String userName) {
//
//    }


    public String role_change(Long id) {
        //받아온 id로 User를 찾는다.
        User user = getByUserId(id);

        //User의 Role을 꺼내기
        UserRole role = user.getRole();

        // 권한을 병경하는 로직
        if(role == UserRole.ROLE_USER) {
            role = UserRole.ROLE_ADMIN;
            user.setRole(role);
        }
        else {
            role = UserRole.ROLE_USER;
            user.setRole(UserRole.ROLE_USER);
        }
        return "권한이 " + role + "으로 변경되었습니다.";
    }

    /**
     *
     * 중복 제거
     *
     */

    //userId로 User찾는 로직 중복제거
    public User getByUserId(Long userId) {
        return userRepository.findById(userId).orElseThrow(() ->
                new AppException(ErrorCode.ID_NOT_FOUND, ErrorCode.ID_NOT_FOUND.getMessage()));
    }
    // 권한을 확인
    public User getUserByUsername(String username) {
        log.info("service username after = {}", username);
        return userRepository.findByUsername(username)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND,ErrorCode.USERNAME_NOT_FOUND.getMessage()));
    }

    private void findByUserName(UserJoinRequest dto) {
        userRepository.findByUsername(dto.getUsername())
                .ifPresent((user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, ErrorCode.DUPLICATED_USER_NAME.getMessage());
                }));
    }
}
