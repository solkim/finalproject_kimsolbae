package com.sns.finalproject_kimsolbae.service;

import com.sns.finalproject_kimsolbae.domain.UserRole;
import com.sns.finalproject_kimsolbae.domain.dto.alarm.AlarmResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostRequest;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.repository.PostRepository;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Transactional
public class PostService {
    private final PostRepository postRepository;

    private final UserRepository userRepository;

    /**
     *게시글 등록
     */
    public PostResponse write(PostRequest dto, String userName) {
        //user 없으면 예외처리
        User user = findByUserName(userName);
        //등록
        Post savedPost = postRepository.save(dto.toEntity(user));
        return PostResponse.of("포스트 등록 완료 ",savedPost.getId());
    }

    public PostResponse write1(PostRequest dto) {
        //등록
        Post savedPost = postRepository.save(new Post(dto.getTitle(),dto.getBody()));
        return PostResponse.of("포스트 등록 완료 ",savedPost.getId());
    }
    /**
     * 전체조회
     */
    public Page<PostOneResponse> getPostsList(Pageable pageable) {
        // Pageing을 해서 담는다.
        Page<Post> list = postRepository.findAll(pageable);
        return list.map(PostOneResponse::fromEntity);
    }

    public Page<PostOneResponse> getPostsListWithSearching(String title, String body, Pageable pageable) {
        Page<Post> list = postRepository.findByTitleContainingOrBodyContaining(title, body, pageable);
        return list.map(PostOneResponse::fromEntity);
    }

    /**
     *게시글 한개 조회
     */
    public PostOneResponse getPostById(Long postsId) {
        // PathVariable로 받은 id로 Post를 찾고 리턴한다.
        Post post = findByPostId(postsId);
        return PostOneResponse.fromEntity(post);
    }

    /**
     *게시글 수정
     */
    public PostResponse modify(Long postId, PostRequest dto, String userName) {
        //작성자와 게시글 중복체크 및 권한 확인
        Post beforePost = checkUserNPostNRole(postId, userName);
        beforePost.setTitle(dto.getTitle());
        beforePost.setBody(dto.getBody());
        return PostResponse.of("포스트 수정 완료", postId);
    }

    public PostResponse modify1(Long postId, PostRequest dto) {
        //작성자와 게시글 중복체크 및 권한 확인
        Post beforePost = findByPostId(postId);
        beforePost.setTitle(dto.getTitle());
        beforePost.setBody(dto.getBody());
        return new PostResponse("good", postId);
    }

    /**
     *게시글 삭제
     */
    public PostResponse delete(Long postId, String userName) {
        //작성자와 게시글 중복체크 및 권한 확인
        Post post = checkUserNPostNRole(postId, userName);
        //softDelete 로직 transactional 로 변경사항이 생기면 자동으로 update쿼리가 전송된다.
        post.deleteSoftly(LocalDateTime.now());
        return PostResponse.of("포스트 삭제 완료", postId);
    }

    /**
     * 중복 함수 제거
     */
    //작성자와 게시글 중복체크 및 권한 확인
    public Post checkUserNPostNRole(Long postId, String userName) {
        //게시글 작성자 꺼내기
        User postUser = findByUserName(userName);
        //id 값으로 게시글 찾기
        Post post = findByPostId(postId);

        if(!(postUser.getRole() == UserRole.ROLE_ADMIN)) { //ADMIN이면 통과
            if (!postUser.getUsername().contains(post.getUser().getUsername())) { //같은 사람이 맞는지 확인
                throw new AppException(ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage());
            }
        }
        return post;
    }

    // Post를 찾는 로직 중복제거
    public Post findByPostId(Long postId) {
        return postRepository.findById(postId).orElseThrow(() ->
                new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage())
        );
    }

    public User findByUserName(String userName) {
        return userRepository.findByUsername(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND,ErrorCode.USERNAME_NOT_FOUND.getMessage()));
    }
}
