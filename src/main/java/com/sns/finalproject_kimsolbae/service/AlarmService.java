package com.sns.finalproject_kimsolbae.service;

import com.sns.finalproject_kimsolbae.domain.dto.alarm.AlarmResponse;
import com.sns.finalproject_kimsolbae.domain.entity.Alarm;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.repository.AlarmRepository;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AlarmService {

    private final AlarmRepository alarmRepository;
    private final UserRepository userRepository;
    public Page<AlarmResponse> getAlarms(Pageable pageable, String userName) {
        User user = findByUserName(userName);
        Page<Alarm> alarms = alarmRepository.findByUser(user, pageable);
        return alarms.map(AlarmResponse::fromEntity);
    }

    public User findByUserName(String userName) {
        return userRepository.findByUsername(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND,ErrorCode.USERNAME_NOT_FOUND.getMessage()));
    }
}
