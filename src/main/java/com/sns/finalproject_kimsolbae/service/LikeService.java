package com.sns.finalproject_kimsolbae.service;

import com.sns.finalproject_kimsolbae.domain.AlarmType;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.domain.entity.Alarm;
import com.sns.finalproject_kimsolbae.domain.entity.Like;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.repository.AlarmRepository;
import com.sns.finalproject_kimsolbae.repository.LikeRepository;
import com.sns.finalproject_kimsolbae.repository.PostRepository;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class LikeService {

    private final LikeRepository likeRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final AlarmRepository alarmRepository;

    public Long getLikes(Long postId) {
        Post post = findByPostId(postId);
        return likeRepository.countByPostId(post.getId());
    }
    public String like(Long postId, String userName) {
        Post post = findByPostId(postId);
        User user = findByUserName(userName);
        return clickLike(post, user);
    }

    //좋아요가 처음인지, 아니면 한번 눌렀었는지
    private String clickLike(Post post, User user) {
        // Optional로 한이유는 null이 반환되면 오류대신 좋아요를 해야하는 처리를 해야하기 때문이다. // 존재한다면 중복처리
        Optional<Like> like = likeRepository.findByPostIdAndUserId(post.getId(), user.getId());
        Optional<Alarm> targetAlarm = alarmRepository.findByTargetIdAndFromUserId(post.getId(), user.getId());
        //좋아요가 있다는 뜻.
        if (like.isPresent()) return likeOrUnLike(like, targetAlarm);
        // 좋아요를 누르지 않았다면 좋아요 저장
        likeRepository.save(new Like(post, user));
        alarmRepository.save(Alarm.toEntity(user, post, AlarmType.NEW_LIKE_ON_POST));
        return "좋아요를 눌렀습니다.";
    }
    //눌렀다면 취소 or 좋아요
    private static String likeOrUnLike(Optional<Like> like, Optional<Alarm> targetAlarm) {
        Like result = like.get();
        if (!result.isSoftDeleted()) { // deleteAt이 null이라면 = 좋아요 이미 누른경우
            result.deleteSoftly(LocalDateTime.now());
            targetAlarm.get().deleteSoftly(LocalDateTime.now());
            return "좋아요를 취소했습니다.";

        } else { //deleteAt이 null이 아니라면 = 좋아요를 취소한경우
            result.undoDeletion();
            targetAlarm.get().undoDeletion();
            return "좋아요를 다시 눌렀습니다.";
        }
    }

    // Post를 찾는 로직 중복제거
    public Post findByPostId(Long postId) {
        return postRepository.findById(postId).orElseThrow(() ->
                new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage())
        );
    }

    public User findByUserName(String userName) {
        return userRepository.findByUsername(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND,ErrorCode.USERNAME_NOT_FOUND.getMessage()));
    }
}
