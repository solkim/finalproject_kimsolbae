package com.sns.finalproject_kimsolbae.service;

import com.sns.finalproject_kimsolbae.domain.dto.myFeed.MyFeedResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.repository.PostRepository;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MyFeedService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;

    public Page<MyFeedResponse> getMyFeeds(Pageable pageable, String userName) {
        User user = findByUserName(userName);
        return postRepository.findAllByUser(user, pageable).map(MyFeedResponse::fromEntity);
    }
    public User findByUserName(String userName) {
        return userRepository.findByUsername(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND,ErrorCode.USERNAME_NOT_FOUND.getMessage()));
    }
}
