package com.sns.finalproject_kimsolbae.service;

import com.sns.finalproject_kimsolbae.domain.AlarmType;
import com.sns.finalproject_kimsolbae.domain.dto.comment.*;
import com.sns.finalproject_kimsolbae.domain.entity.*;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CommentService {
    private final CommentRepository commentRepository;
    private final AlarmRepository alarmRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;


    public CommentResponse write(Long postId,String userName ,CommentRequest commentRequest) {
        User user = findByUserName(userName);
        Post post = findByPostId(postId);
        // 저장
        Comment comment = commentRepository.save(commentRequest.toEntity(user, post));
        //알람 저장
        alarmRepository.save(Alarm.toEntity(user, post, AlarmType.NEW_COMMENT_ON_POST));
        return CommentResponse.fromEntity(comment);
    }

    public ReplyCommentResponseDto writeReplyComment(ReplyCommentRequestDto requestDto, String userName, Long postId, Long parentCommentId) {
        User user = findByUserName(userName);
        Post post = findByPostId(postId);
        Comment parentComment = getComment(parentCommentId);

        Comment comment = commentRepository.save(Comment.createReplyComment(requestDto.getReplyComment(), user, post, parentComment));
        alarmRepository.save(Alarm.toEntity(user, post, AlarmType.NEW_COMMENT_ON_POST));

        return new ReplyCommentResponseDto(comment, userName, postId, parentCommentId);
    }

    private Comment getComment(Long parentCommentId) {
         return commentRepository.findById(parentCommentId).orElseThrow(() ->
                new AppException(ErrorCode.COMMENT_NOT_FOUND, ErrorCode.COMMENT_NOT_FOUND.getMessage())
        );
    }

    //댓글조회
    public Page<CommentResponse> getCommentList(Pageable pageable, Long postId) {
        //postId로 찾아 리스트를 조회한다.
        Page<Comment> list = commentRepository.findByPostId(postId, pageable);
        return list.map(CommentResponse::fromEntity);
    }

    //대댓글 조회
    public Page<CommentResponse> getCommentReplyList(Pageable pageable, Long postId, Long parentCommentId) {
        //postId로 찾아 리스트를 조회한다.
        Page<Comment> list = commentRepository.findByPostIdAndParentCommentId(postId, pageable, parentCommentId);
        return list.map(CommentResponse::fromEntity);
    }

    public CommentResponse modify(CommentRequest commentRequest, Long postId, Long commentId, String userName) {
        Comment comment = checkCommentAndPost(postId, commentId);

        //같은 유저가 작성한 것인지 확인
        if (!comment.getUser().getUsername().equals(userName)){
            throw new AppException(ErrorCode.ID_NOT_MATCH, ErrorCode.ID_NOT_MATCH.getMessage());
        }
        comment.setComment(commentRequest.getComment());
        return CommentResponse.fromEntity(comment);
    }



    public CommentDeleteResponse delete(Long postId, Long commentId, String userName) {

        //Comment를 찾는다.
        Comment comment = checkCommentAndPost(postId, commentId);

        // comment를 작성한 작성자가 맞는지 확인
        if(!comment.getUser().getUsername().equals(userName) ){
            throw new AppException(ErrorCode.ID_NOT_MATCH, ErrorCode.ID_NOT_MATCH.getMessage());
        }

        //삭제
        comment.deleteSoftly(LocalDateTime.now());
        return CommentDeleteResponse.of(commentId);
    }


    private Comment checkCommentAndPost(Long postId, Long commentId) {
        //Comment를 찾는다.
        Comment comment = commentRepository.findById(commentId).orElseThrow(() ->
                new AppException(ErrorCode.COMMENT_NOT_FOUND, ErrorCode.COMMENT_NOT_FOUND.getMessage()));
        //Post가 있는지 확인
        findByPostId(postId);
        return comment;
    }


    // Post를 찾는 로직 중복제거
    public Post findByPostId(Long postId) {
        return postRepository.findById(postId).orElseThrow(() ->
                new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage())
        );
    }

    public User findByUserName(String userName) {
        return userRepository.findByUsername(userName)
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND,ErrorCode.USERNAME_NOT_FOUND.getMessage()));
    }
}
