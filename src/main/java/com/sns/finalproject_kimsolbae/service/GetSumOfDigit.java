package com.sns.finalproject_kimsolbae.service;

import org.springframework.stereotype.Service;

@Service
public class GetSumOfDigit {

    public Long sumOfDigit(Long num) {
        Long sum = 0L;
        while (num > 0) {
            sum += num % 10;
            num = num / 10;
        }
        return sum;
    }
}
