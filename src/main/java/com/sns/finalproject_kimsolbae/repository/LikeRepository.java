package com.sns.finalproject_kimsolbae.repository;

import com.sns.finalproject_kimsolbae.domain.entity.Like;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface LikeRepository extends JpaRepository<Like, Long> {
    // 좋아요가 삭제였다면 다시 눌러야 하기때문에 where에 조건을 무시한다.
    @Query(value = "SELECT * FROM likes where post_id=:postId and user_id=:userId", nativeQuery=true)

    Optional<Like> findByPostIdAndUserId(@Param("postId") Long postId, @Param("userId") Long userId);

    Long countByPostId(Long postId);
}
