package com.sns.finalproject_kimsolbae.repository;

import com.sns.finalproject_kimsolbae.domain.entity.RedisToken;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface RedisTokenRepository extends CrudRepository<RedisToken, Long> {
}
