package com.sns.finalproject_kimsolbae.repository;

import com.sns.finalproject_kimsolbae.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
