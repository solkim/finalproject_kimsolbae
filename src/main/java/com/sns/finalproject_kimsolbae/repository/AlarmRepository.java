package com.sns.finalproject_kimsolbae.repository;

import com.sns.finalproject_kimsolbae.domain.entity.Alarm;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AlarmRepository extends JpaRepository<Alarm, Long> {
    Page<Alarm> findByUser(User user, Pageable pageable);

    Optional<Alarm> findByTargetIdAndFromUserId(Long postId, Long userId);
}
