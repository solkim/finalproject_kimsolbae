package com.sns.finalproject_kimsolbae.repository;

import com.sns.finalproject_kimsolbae.domain.entity.Comment;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    Page<Comment> findByPostId(Long id, Pageable pageable);
    Page<Comment> findByPostIdAndParentCommentId(Long id, Pageable pageable, Long parentCommentId);
}
