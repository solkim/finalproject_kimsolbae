package com.sns.finalproject_kimsolbae.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.config.auth.PrincipalDetails;
import com.sns.finalproject_kimsolbae.domain.dto.token.TokenResponseDto;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserLoginResponse;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class JwtUtil {
    private static final long ACCESS_TOKEN_EXPIRE_TIME = 10 * 60 * 1000L; // 10분
    private static final long REFRESH_TOKEN_EXPIRE_TIME = 14 * 24 * 60 * 60 * 1000L;    // 14일

    // logout 시 토큰 정보를 검증하는 메서드
    public static boolean validateToken(String token, String key) throws MalformedJwtException {
        try {
            Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getExpiration();
            return true;
        } catch (ExpiredJwtException e) {
            log.info("Expired JWT Token", e);
        } catch (UnsupportedJwtException e) {
            log.info("Unsupported JWT Token", e);
        } catch (IllegalArgumentException e) {
            log.info("JWT claims string is empty.", e);
        }
        return false;
    }

    public static String getUserName(String token, String secretKey) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token)
                .getBody().get("username", String.class);
    }
    public static boolean isExpired(String token, String secretKey) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token)
                .getBody().getExpiration().before(new Date());
    }

    public static Long getExpiration(String accessToken, String key) {
        // accessToken 남은 유효시간
        Date expiration = Jwts.parser().setSigningKey(key).parseClaimsJws(accessToken).getBody().getExpiration();
        // 현재 시간
        Long now = new Date().getTime();
        return (expiration.getTime() - now);
    }


    public static String createToken(PrincipalDetails principalDetails, String key, Long expiredTime) throws JsonProcessingException {
        Claims claims = Jwts.claims(); //일종의 map
        claims.put("username", principalDetails.getUsername());
        claims.put("role", principalDetails.getAuthorities());

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expiredTime))
                .signWith(SignatureAlgorithm.HS256,key)
                .compact()
                ;
    }

    public static TokenResponseDto createTokenByLogin(User user, String secretKey) throws JsonProcessingException {

        Subject atkSubject = Subject.atk(user.getId(), user.getUsername(), user.getRole().toString());
        Subject rtkSubject = Subject.rtk(user.getId(), user.getUsername());

        String atk = createToken(new PrincipalDetails(user), secretKey, ACCESS_TOKEN_EXPIRE_TIME);
        String rtk = createToken(new PrincipalDetails(user), secretKey, REFRESH_TOKEN_EXPIRE_TIME);

        return new TokenResponseDto(atk,rtk);
    }
    //reissue 처리해야한다.
    public TokenResponseDto Atkreissue(UserLoginResponse userLoginResponse) {
        return null;
    }
}
