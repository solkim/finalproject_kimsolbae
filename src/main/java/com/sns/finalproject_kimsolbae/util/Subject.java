package com.sns.finalproject_kimsolbae.util;

import lombok.Getter;

@Getter
public class Subject {

    private Long accountId;

    private String username;
    private String type;
    private String role;

    private Subject(Long accountId, String username, String type, String role) {
        this.accountId = accountId;
        this.username = username;
        this.type = type;
        this.role = role;
    }
    private Subject(Long accountId, String username, String type) {
        this.accountId = accountId;
        this.username = username;
        this.type = type;
    }

    public static Subject atk(Long accountId, String username, String role) {
        return new Subject(accountId, username, "AT", role);
    }

    public static Subject rtk(Long accountId, String username) {
        return new Subject(accountId, username, "RT");
    }
}
