package com.sns.finalproject_kimsolbae.controller.restcontroller;

import com.sns.finalproject_kimsolbae.service.GetSumOfDigit;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/hello")
@RequiredArgsConstructor
//@ApiIgnore
@Api(tags = {"Hello API"})
public class HelloRestController {

    private final GetSumOfDigit getSumOfDigit;

    @GetMapping
    @ApiOperation(value = "CI/CD Test",notes = "매일 return 값 변경하여 Test 해보기")
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok().body("김솔배");
    }

    @GetMapping("/{num}")
    @ApiOperation(value = "정수의 자리수 더하는 로직", notes = "정수를 입력")
    public ResponseEntity<Long> sumOfDigit(@PathVariable Long num) {
        return ResponseEntity.ok().body(getSumOfDigit.sumOfDigit(num));
    }

}