package com.sns.finalproject_kimsolbae.controller.restcontroller;

import com.sns.finalproject_kimsolbae.config.auth.PrincipalDetails;
import com.sns.finalproject_kimsolbae.domain.Response;
import com.sns.finalproject_kimsolbae.domain.dto.alarm.AlarmResponse;
import com.sns.finalproject_kimsolbae.service.AlarmService;
import com.sns.finalproject_kimsolbae.service.PostService;
import com.sns.finalproject_kimsolbae.util.Subject;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/alarms")
@Api(tags = {"AlarmController API"})
public class AlarmController {
    private final AlarmService alarmService;
    @GetMapping
    public Response<Page<AlarmResponse>> alarms(@PageableDefault(size = 20, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable, @AuthenticationPrincipal PrincipalDetails authentication) {
        return Response.success(alarmService.getAlarms(pageable, authentication.getUsername()));
    }
}
