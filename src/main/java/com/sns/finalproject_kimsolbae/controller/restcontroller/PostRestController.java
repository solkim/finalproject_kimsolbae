package com.sns.finalproject_kimsolbae.controller.restcontroller;

import com.sns.finalproject_kimsolbae.config.auth.PrincipalDetails;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostRequest;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.domain.Response;
import com.sns.finalproject_kimsolbae.service.PostService;
import com.sns.finalproject_kimsolbae.util.Subject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/posts")
@RestController
@RequiredArgsConstructor
@Api(tags = {"Post API"})
@Slf4j
public class PostRestController {

    private final PostService postService;

    //등록
    @ApiOperation(value = "게시글 작성기능",notes = "로그인 한 사용자가 title,body 값으로 게시글 작성")
    @PostMapping("")
    public Response<PostResponse> writePost(@RequestBody PostRequest dto, @AuthenticationPrincipal PrincipalDetails authentication) {
        log.info("\nuserDetails name = {} role = {}\n", authentication.getUsername(), authentication.getUser().getRole());
        return Response.success(postService.write(dto, authentication.getUsername()));
    }

    //list 목록 최신순
    @ApiOperation(value = "게시글 전체조회기능")
    @GetMapping("")
    public Response<Page<PostOneResponse>> getPosts(@PageableDefault(size = 20, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable){
        return Response.success(postService.getPostsList(pageable));
    }

    //한개 조회
    @ApiOperation(value = "게시글 상세조회기능")
    @GetMapping("/{id}")
    public Response<PostOneResponse> getPost(@PathVariable Long id){
        return Response.success(postService.getPostById(id));
    }

    //게시글 수정
    @ApiOperation(value = "게시글 수정기능",notes = "로그인 한 사용자가 title,body 값으로 게시글 수정")
    @PutMapping("/{id}")
    public Response<PostResponse> modify(@PathVariable Long id, @RequestBody PostRequest postRequest, @AuthenticationPrincipal PrincipalDetails authentication) {
        return Response.success(postService.modify(id, postRequest, authentication.getUsername()));
    }

    //게시글 삭제
    @ApiOperation(value = "게시글 삭제기능")
    @DeleteMapping("/{id}")
    public Response<PostResponse> delete(@PathVariable Long id, @AuthenticationPrincipal PrincipalDetails authentication) {
        return Response.success(postService.delete(id, authentication.getUsername()));
    }
}