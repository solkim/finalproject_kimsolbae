package com.sns.finalproject_kimsolbae.controller.restcontroller;


import com.sns.finalproject_kimsolbae.config.auth.PrincipalDetails;
import com.sns.finalproject_kimsolbae.domain.Response;
import com.sns.finalproject_kimsolbae.domain.dto.comment.*;
import com.sns.finalproject_kimsolbae.service.CommentService;
import com.sns.finalproject_kimsolbae.util.Subject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/posts")
@Api(tags = {"Comment API"})
public class CommentController {
    private final CommentService commentService;

    //댓글 작성
    @PostMapping("/{postsId}/comments")
    @ApiOperation(value = "댓글 작성기능",notes = "로그인 한 사용자가 포스트에 댓글 작성")
    //댓글 작성은 로그인 한 사람만 쓸 수 있습니다.
    public Response<CommentResponse> write(@PathVariable Long postsId, @RequestBody CommentRequest commentRequest,@AuthenticationPrincipal PrincipalDetails authentication) {
        //Subject타입으로 넣어놓았으니 Subject로 캐스팅 해주어야 한다.
       return Response.success(commentService.write(postsId, authentication.getUsername(),commentRequest));
    }

    //대댓글 작성
    @PostMapping("/{postsId}/comments/{commentId}")
    @ApiOperation(value = "대댓글 작성기능",notes = "로그인 한 사용자가 포스트 댓글의 댓글 작성")
    //댓글 작성은 로그인 한 사람만 쓸 수 있습니다.
    public Response<ReplyCommentResponseDto> writeReplyComment(@PathVariable Long postsId, @RequestBody ReplyCommentRequestDto replyCommentResponseDto, @PathVariable Long commentId, @AuthenticationPrincipal PrincipalDetails authentication) {
        //Subject타입으로 넣어놓았으니 Subject로 캐스팅 해주어야 한다.
        return Response.success(commentService.writeReplyComment(replyCommentResponseDto, authentication.getUsername(),postsId, commentId));
    }

    //댓글 전체 조회
    @GetMapping("/{postId}/comments")
    @ApiOperation(value = "댓글 전체 조회")
    public Response<Page<CommentResponse>> getComments(@PageableDefault(size = 10, sort = {"id"}, direction = Sort.Direction.DESC)
                                                           Pageable pageable, @PathVariable Long postId) {
        return Response.success(commentService.getCommentList(pageable, postId));
    }
    //대댓글 전체 조회
    @GetMapping("/{postId}/comments/{commentId}")
    @ApiOperation(value = "대댓글 전체 조회")
    public Response<Page<CommentResponse>> getComments(@PageableDefault(size = 10, sort = {"id"}, direction = Sort.Direction.DESC)
                                                       Pageable pageable, @PathVariable Long postId, @PathVariable Long commentId) {
        return Response.success(commentService.getCommentReplyList(pageable, postId, commentId));
    }

    //댓글 수정
    @PutMapping("/{postId}/comments/{id}")
    @ApiOperation(value = "댓글 수정", notes = "댓글 작성자만 댓글 수정")
    public Response<CommentResponse> modify(@RequestBody CommentRequest commentRequest, @PathVariable Long postId, @PathVariable Long id,
                                            @AuthenticationPrincipal PrincipalDetails authentication) {
        //Subject타입으로 넣어놓았으니 Subject로 캐스팅 해주어야 한다.
//        Subject auth = (Subject) authentication.getPrincipal();
//        log.info("\nauth = {}\n", auth.getUsername()());

        return Response.success(commentService.modify(commentRequest, postId, id, authentication.getUsername()));
    }
    //댓글 삭제
    @DeleteMapping("/{postId}/comments/{id}")
    @ApiOperation(value = "댓글 삭제")
    public Response<CommentDeleteResponse> delete(@PathVariable Long postId, @PathVariable Long id, @AuthenticationPrincipal PrincipalDetails authentication) {
        return Response.success(commentService.delete(postId, id, authentication.getUsername()));
    }
}
