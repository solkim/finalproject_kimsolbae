package com.sns.finalproject_kimsolbae.controller.restcontroller;

import com.sns.finalproject_kimsolbae.config.auth.PrincipalDetails;
import com.sns.finalproject_kimsolbae.domain.Response;
import com.sns.finalproject_kimsolbae.domain.dto.myFeed.MyFeedResponse;
import com.sns.finalproject_kimsolbae.service.MyFeedService;
import com.sns.finalproject_kimsolbae.util.Subject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1/posts")
@RestController
@RequiredArgsConstructor
@Api(tags = {"MyFeed API"})
@Slf4j
public class MyFeedRestController {
    private final MyFeedService myFeedService;
    @ApiOperation(value = "마이 피드",notes = "로그인 한 사용자 글만 조회")
    @GetMapping("/my")
    public Response<Page<MyFeedResponse>> myFeed(@PageableDefault (size = 20, sort = {"id"}, direction = Sort.Direction.DESC) Pageable  pageable, @AuthenticationPrincipal PrincipalDetails authentication) {
        return Response.success(myFeedService.getMyFeeds(pageable, authentication.getUsername()));
    }
}