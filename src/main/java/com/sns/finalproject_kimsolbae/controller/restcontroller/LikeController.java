package com.sns.finalproject_kimsolbae.controller.restcontroller;

import com.sns.finalproject_kimsolbae.config.auth.PrincipalDetails;
import com.sns.finalproject_kimsolbae.domain.Response;
import com.sns.finalproject_kimsolbae.service.LikeService;
import com.sns.finalproject_kimsolbae.util.Subject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/posts")
@RestController
@RequiredArgsConstructor
@Api(tags = {"Like API"})
@Slf4j
public class LikeController {

    private final LikeService likeService;

    @PostMapping("/{postId}/likes")
    @ApiOperation(value = "좋아요 기능", notes = "게시글에(postId) 좋아요 표시")
    public Response<String> likes(@PathVariable Long postId, @AuthenticationPrincipal PrincipalDetails authentication) {
        return Response.success(likeService.like(postId, authentication.getUsername()));
    }

    @GetMapping("/{postsId}/likes")
    @ApiOperation(value = "좋아요 조회 기능", notes = "게시글에(postsId) 달린 좋아요 조회")
    public Response<Long> getLikes(@PathVariable Long postsId) {
        return Response.success(likeService.getLikes(postsId));
    }
}
