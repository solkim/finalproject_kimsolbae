package com.sns.finalproject_kimsolbae.controller.restcontroller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.sns.finalproject_kimsolbae.domain.Response;
import com.sns.finalproject_kimsolbae.domain.dto.token.TokenResponseDto;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinRequest;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinResponse;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserLoginRequest;
import com.sns.finalproject_kimsolbae.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Slf4j
@Api(tags = {"User API"})
public class UserRestController {

    private final UserService userService;
    @ApiOperation(value = "회원가입 기능",notes = "userName, password 입력해서 회원가입")
    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest userJoinRequest) {
        log.info("\nusername = {}, password = {}\n",userJoinRequest.getUsername(), userJoinRequest.getPassword());
        //id에 sol라는 단어가 들어가면 ADMIN 권한이 주어진다.
        return Response.success(userService.join(userJoinRequest));
    }
    @ApiOperation(value = "로그인 기능",notes = "회원가입한 정보로 로그인")
    @CrossOrigin(exposedHeaders = "Authorization")
    @PostMapping("/login")
    public Response<TokenResponseDto> login(@RequestBody UserLoginRequest userLoginRequest, HttpServletResponse response) throws JsonProcessingException {
        log.info("id : {} password : {} 들어왔는데??\n", userLoginRequest.getUsername(),userLoginRequest.getPassword());
//        response.addHeader("Authorization", "Bearer " + "되나?");
        TokenResponseDto tokens = userService.login(userLoginRequest);

        response.addHeader("Authorization", "dfse");
        Cookie cookie = new Cookie("Authorization", "Bearer+" + tokens.getAccessToken());
        cookie.setPath("/");
        response.addCookie(cookie);
        return Response.success(tokens);
    }

    @PostMapping("/logout")
    @ApiOperation(value = "로그아웃 기능",notes = "로그인한 유저 로그아웃")
    public Response<String> logout(@RequestBody TokenResponseDto tokenResponseDto) throws JsonProcessingException {
        return Response.success(userService.logout(tokenResponseDto));
    }

    //권한을 변경하는 로직
    @ApiOperation(value = "권한 기능",notes = "ADMIN 계정이 사용자 권한 설정")
    @PostMapping("/{id}/role/change")
    public Response role_change(@PathVariable Long id) {
        return Response.success(userService.role_change(id));
    }
}
