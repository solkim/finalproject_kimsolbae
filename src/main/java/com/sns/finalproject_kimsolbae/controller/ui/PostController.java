package com.sns.finalproject_kimsolbae.controller.ui;

import com.sns.finalproject_kimsolbae.config.auth.PrincipalDetails;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostRequest;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import com.sns.finalproject_kimsolbae.service.PostService;
import com.sns.finalproject_kimsolbae.validator.PostValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/posts")
@Slf4j
public class PostController {

    private final PostService postService;
    private final PostValidator postValidator;
    @GetMapping("/list")
    public String list(Model model, @PageableDefault(size = 10, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable, @RequestParam(required = false, defaultValue = "") String searchText) {
        Page<PostOneResponse> postsList = postService.getPostsListWithSearching(searchText, searchText, pageable);

        int startPage = Math.max(1, postsList.getPageable().getPageNumber() - 4);
        int endPage = Math.min(postsList.getTotalPages(), postsList.getPageable().getPageNumber() + 4);
        model.addAttribute("startPage", startPage);
        model.addAttribute("endPage", endPage);
        model.addAttribute("posts", postsList);
        return "posts/list";
    }

    //글쓰기
    @GetMapping("/write")
    public String form(Model model, @RequestParam(required = false) Long id) {
        if (id == null) {
            model.addAttribute("postRequest", new PostRequest());
        } else {
            Post post = postService.findByPostId(id);
            model.addAttribute("post", post);
            return "posts/modify";
        }
        return "posts/write";
    }
    @PostMapping("/write")
    public String writePost(@Valid PostRequest postRequest, BindingResult bindingResult) {
        postValidator.validate(postRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            return "posts/write";
        }
        postService.write1(postRequest);
        return "redirect:/posts/list";
    }

    @PostMapping("/modify/{id}")
    public String modify(@PathVariable(value="id") Long id, @Valid Post post, BindingResult bindingResult){
        postValidator.validatePost(post, bindingResult);
        if (bindingResult.hasErrors()) {
            return "posts/write";
        }
        PostRequest postRequest = new PostRequest(post.getTitle(),post.getBody());
        postService.modify1(id, postRequest);
        return "redirect:/posts/list";
    }

}
