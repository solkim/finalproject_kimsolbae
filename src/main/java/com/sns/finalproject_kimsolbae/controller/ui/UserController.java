package com.sns.finalproject_kimsolbae.controller.ui;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sns.finalproject_kimsolbae.config.auth.JwtProperties;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostRequest;
import com.sns.finalproject_kimsolbae.domain.dto.token.TokenResponseDto;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinRequest;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserLoginRequest;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Controller
@RequiredArgsConstructor
@RequestMapping("/users")
@Slf4j
public class UserController {

    private final UserService userService;

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("userLoginRequest", new UserLoginRequest());
        log.info("login start");
        return "users/login";
    }
    @PostMapping("/login")
    public String loginPost(UserLoginRequest userLoginRequest, HttpServletRequest request) throws JsonProcessingException {
        log.info("name = {}, password = {}",userLoginRequest.getUsername(),userLoginRequest.getPassword());
        TokenResponseDto loginToken = userService.login(userLoginRequest);

        //기존 Session 파기 후 새로 생성
        request.getSession().invalidate();
        HttpSession session = request.getSession(true);

        // login 후 받은 jwt 값을 session에 넣어줌
        session.setAttribute(JwtProperties.HEADER_STRING, JwtProperties.TOKEN_PREFIX + loginToken.getAccessToken());
        session.setMaxInactiveInterval(1800); // Seesion 30분 유지

        return "redirect:/posts/list";
    }

    @GetMapping("/join")
    public String join(Model model) {
        model.addAttribute("userJoinRequest", new UserJoinRequest());
        log.info("login start");
        return "users/join";
    }
    @PostMapping("/join")
    public String join(UserJoinRequest userJoinRequest) {
        userService.join(userJoinRequest);
        return "redirect:/users/login";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.removeAttribute(JwtProperties.HEADER_STRING);
        return "redirect:/posts/list";
    }

}
