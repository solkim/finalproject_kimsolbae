package com.sns.finalproject_kimsolbae.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.domain.dto.ErrorResponse;
import com.sns.finalproject_kimsolbae.domain.Response;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestControllerAdvice
public class ExceptionManager {
    @ExceptionHandler(AppException.class)
    public ResponseEntity<Response> AppExceptionHandler(AppException e) {
        return ResponseEntity.status(e.getErrorCode().getHttpStatus()).
                body(Response.error(new ErrorResponse(e.getErrorCode().name(), e.getMessage())));
    }
}
