package com.sns.finalproject_kimsolbae.service;

import com.sns.finalproject_kimsolbae.domain.AlarmType;
import com.sns.finalproject_kimsolbae.domain.entity.Alarm;
import com.sns.finalproject_kimsolbae.domain.entity.Like;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.fixture.PostEntityFixture;
import com.sns.finalproject_kimsolbae.fixture.TestInfoFixture;
import com.sns.finalproject_kimsolbae.fixture.UserEntityFixture;
import com.sns.finalproject_kimsolbae.repository.AlarmRepository;
import com.sns.finalproject_kimsolbae.repository.LikeRepository;
import com.sns.finalproject_kimsolbae.repository.PostRepository;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

class LikeServiceTest {

    private LikeRepository likeRepository = Mockito.mock(LikeRepository.class);
    private PostRepository postRepository = Mockito.mock(PostRepository.class);
    private UserRepository userRepository = Mockito.mock(UserRepository.class);
    private AlarmRepository alarmRepository = Mockito.mock(AlarmRepository.class);

    private LikeService likeService;
    private TestInfoFixture.TestInfo fixture;
    private Optional<Like> like;
    private Optional<Alarm> alarm;

    @BeforeEach
    void setUp() {
        likeService = new LikeService(likeRepository, userRepository, postRepository,alarmRepository);
        fixture = TestInfoFixture.get();
        like = Optional.ofNullable(Like.builder()
                .id(fixture.getLikeId())
                .user(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword()))
                .post(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword()))
                .build());
        alarm = Optional.of(Alarm.builder()
                .id(fixture.getAlarmId())
                .user(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword()))
                .text("성공")
                .targetId(fixture.getPostId())
                .fromUserId(fixture.getUserId())
                .alarmType(AlarmType.NEW_COMMENT_ON_POST)
                .build());
    }


    /**
     * 좋아요 기능
     */
    @Test
    @DisplayName("좋아요 성공")
    void like_success() {
        //given
        given(postRepository.findById(any())).willReturn(Optional.ofNullable(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword())));

        //좋아요를 누르지 않았다면 비어있는 값이 리턴
        Optional<Like> like = Optional.empty();
        given(likeRepository.findByPostIdAndUserId(any(), any())).willReturn(like);
        Optional<Alarm> alarm = Optional.empty();
        given(alarmRepository.findByTargetIdAndFromUserId(any(), any())).willReturn(alarm);

        //when
        String message = likeService.like(fixture.getPostId(), fixture.getUsername());
        //then
        assertDoesNotThrow(() -> likeService.like(fixture.getPostId(), fixture.getUsername()));
        assertEquals(message,"좋아요를 눌렀습니다.");
    }

    @Test
    @DisplayName("좋아요 성공 - 이미 눌렀다면 좋아요 취소")
    void like_success_좋아요_취소() {
        //given
        given(postRepository.findById(any())).willReturn(Optional.ofNullable(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword())));

        //좋아요를 눌렀다면 좋아요 취소
        given(likeRepository.findByPostIdAndUserId(any(), any())).willReturn(like);
        given(alarmRepository.findByTargetIdAndFromUserId(any(), any())).willReturn(alarm);

        //when
        String message = likeService.like(fixture.getPostId(), fixture.getUsername());
        //then
        assertDoesNotThrow(() -> likeService.like(fixture.getPostId(), fixture.getUsername()));
        assertEquals(message,"좋아요를 취소했습니다.");
    }

    @Test
    @DisplayName("좋아요 성공 - 취소 -> 좋아요 다시 누르기")
    void like_success_다시_좋아요() {
        //given
        given(postRepository.findById(any())).willReturn(Optional.ofNullable(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword())));

        //좋아요를 눌렀다면 좋아요 취소
        Like like1 = Like.builder()
                .id(fixture.getLikeId())
                .user(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword()))
                .post(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword()))
                .build();
        like1.setDeletedAt(LocalDateTime.now());
        Optional<Like> like11 = Optional.ofNullable(like1);

        given(likeRepository.findByPostIdAndUserId(any(), any())).willReturn(like11);
        given(alarmRepository.findByTargetIdAndFromUserId(any(), any())).willReturn(alarm);

        //when
        String message = likeService.like(fixture.getPostId(), fixture.getUsername());
        //then
        assertDoesNotThrow(() -> likeService.like(fixture.getPostId(), fixture.getUsername()));
        assertEquals(message,"좋아요를 다시 눌렀습니다.");
    }

    /**
     * 좋아요 기능 실패
     */

    @Test
    @DisplayName("좋아요 실패 - user없음")
    void like_fail_user없음() {
        //given
        given(postRepository.findById(any())).willReturn(Optional.ofNullable(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        //user가 없는경우
        given(userRepository.findByUsername(any())).willThrow(new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));
        given(likeRepository.findByPostIdAndUserId(any(), any())).willReturn(like);

        //when
        AppException e = assertThrows(AppException.class, () -> likeService.like(fixture.getPostId(), fixture.getUsername()));
        //then
        assertEquals(e.getErrorCode(), ErrorCode.USERNAME_NOT_FOUND);
        assertEquals(e.getMessage(), ErrorCode.USERNAME_NOT_FOUND.getMessage());
    }
    @Test
    @DisplayName("좋아요 실패 - post없음")
    void like_fail_post없음() {
        //given
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(postRepository.findById(any())).willThrow(new AppException(ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        given(likeRepository.findByPostIdAndUserId(any(), any())).willReturn(like);

        //when
        AppException e = assertThrows(AppException.class, () -> likeService.like(fixture.getPostId(), fixture.getUsername()));
        //then
        assertEquals(e.getErrorCode(), ErrorCode.POST_NOT_FOUND);
        assertEquals(e.getMessage(), ErrorCode.POST_NOT_FOUND.getMessage());
    }

    /**
     *
     * 좋아요 조회
     *
     */

    @Test
    @DisplayName("좋아요 조회 성공")
    void getLikes_success() {

        //given
        given(postRepository.findById(any())).willReturn(Optional.ofNullable(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(likeRepository.countByPostId(any())).willReturn(1L);

        //when
         Long likes = likeService.getLikes(1L);

        //then
        assertDoesNotThrow(() -> likeRepository.countByPostId(1L));
        assertEquals(likes, 1L);
    }

    @Test
    @DisplayName("좋아요 조회 실패 - post없음")
    void getLikes_fail() {

        //given
        given(postRepository.findById(any())).willThrow(new AppException(
                ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()
        ));

        //when
        AppException e = assertThrows(AppException.class, () -> likeService.getLikes(any()));

        //then
        assertEquals(e.getErrorCode(),ErrorCode.POST_NOT_FOUND);
        assertEquals(e.getMessage(), ErrorCode.POST_NOT_FOUND.getMessage());
    }
}