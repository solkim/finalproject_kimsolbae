package com.sns.finalproject_kimsolbae.service;

import com.sns.finalproject_kimsolbae.domain.UserRole;
import com.sns.finalproject_kimsolbae.domain.entity.Like;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.repository.PostRepository;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional //Test에 Transactional을 붙이면 끝나면 롤백된다.?
@Rollback(value = false)
@ActiveProfiles("test") // local db에 row가 있는지 직접 확인 하고 싶을 때 쓴다
@Slf4j
class PostServiceWithSpringTest {

    @Autowired
    PostRepository postRepository;
    @Autowired
    UserRepository userRepository;

    User user;
    Post post;

    @BeforeEach
    void setUp() {
        user = User.builder()
                .username("userName")
                .password("password")
                .role(UserRole.ROLE_USER)
                .build();
        post = Post.builder()
                .title("test")
                .body("softDeleteTest")
                .user(null)
                .build();
    }

    @Test
    @DisplayName("soft delete 테스트")
    void post_softDelete_success() {
        //user 저장
        User saveUser = userRepository.save(user);
        //Post 생성
        post.setUser(saveUser);
        //글쓰기
        Post savePost = postRepository.save(post);

        //게시글 확인
        log.info("\n" + "count" + "\n");
        assertEquals(postRepository.count(),1);
        assertEquals(savePost.isSoftDeleted(),false);
        assertNull(savePost.getDeletedAt());

        //게시글 삭제
        log.info("\n" + "deleteSoftly" + "\n");
        savePost.deleteSoftly(LocalDateTime.now());
//        postRepository.save(savePost);

        //게시글 삭제 확인
        log.info("\n" + "count 2" + "\n");
        assertEquals(postRepository.count(),0);
        assertEquals(savePost.isSoftDeleted(),true);
        assertNotNull(savePost.getDeletedAt()); //삭제가 되었다는 뜻
    }
}