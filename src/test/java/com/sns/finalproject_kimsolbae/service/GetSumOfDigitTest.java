package com.sns.finalproject_kimsolbae.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//Annotation 없이 만든다.
class GetSumOfDigitTest {

    //Spring을 안쓰고 테스트 하기 때문에 new를 이용해 초기화를 해준다.
    //Pojo방식을 최대한 활용한다.
    GetSumOfDigit getSumOfDigit = new GetSumOfDigit();

    @Test
    @DisplayName("자리수 합 잘 구하는지")
    void sumOfDigit() {
        assertEquals(21, getSumOfDigit.sumOfDigit(687L));
        assertEquals(22, getSumOfDigit.sumOfDigit(787L));
        assertEquals(0, getSumOfDigit.sumOfDigit(0L));
        assertEquals(5, getSumOfDigit.sumOfDigit(11111L));
    }
}