package com.sns.finalproject_kimsolbae.service;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostRequest;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostResponse;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.fixture.PostEntityFixture;
import com.sns.finalproject_kimsolbae.fixture.TestInfoFixture;
import com.sns.finalproject_kimsolbae.fixture.UserEntityFixture;
import com.sns.finalproject_kimsolbae.repository.PostRepository;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import static org.mockito.ArgumentMatchers.any;
import java.util.Optional;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
class PostServiceTest {
    private PostRepository postRepository = Mockito.mock(PostRepository.class);
    private UserRepository userRepository = Mockito.mock(UserRepository.class);
    private TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
    PostService postService = new PostService(postRepository, userRepository);
    PostRequest postRequest = new PostRequest(fixture.getTitle(), fixture.getBody());

    private static User userA;
    private static Post postA;

    public static Stream<Arguments> provideErrorCases() {
        //beforeSetUp이 작동하지 않는다?
        User userA = UserEntityFixture.get("user a", "1");
        Post postA = PostEntityFixture.get("user a", "1");
        Post differentUserNamePost = PostEntityFixture.get("different", "1");

        return Stream.of(
                Arguments.of(Named.of("작성자 불일치", ErrorCode.INVALID_PERMISSION), Optional.of(differentUserNamePost), Optional.of(userA), userA.getUsername()),
                Arguments.of(Named.of("유저 없음", ErrorCode.USERNAME_NOT_FOUND), Optional.of(postA), Optional.empty(), userA.getUsername()),
                Arguments.of(Named.of("포스트 존재하지 않음", ErrorCode.POST_NOT_FOUND), Optional.empty(), Optional.of(userA), userA.getUsername())
        );
    }

    @ParameterizedTest
    @MethodSource("provideErrorCases")
    @DisplayName("수정, 삭제 실패")
    void fail_edit_comment(ErrorCode errorCode, Optional<Post> post, Optional<User> user, String userName) {

        //given
        given(userRepository.findByUsername(any())).willReturn(user);
        given(postRepository.findById(any())).willReturn(post);

        //when
        AppException modifyError = assertThrows(AppException.class, () -> postService.modify(fixture.getPostId(), postRequest, userName));
        AppException deleteError = assertThrows(AppException.class, () -> postService.delete(fixture.getPostId(), userName));

        //then
        assertEquals(modifyError.getErrorCode(), errorCode);
        assertEquals(modifyError.getMessage(), errorCode.getMessage());

        assertEquals(deleteError.getErrorCode(), errorCode);
        assertEquals(deleteError.getMessage(), errorCode.getMessage());
    }

    /**
     * 포스트 등록
     */
    @Test
    @DisplayName("게시물 등록 성공")
    void post_success() {
        // given
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(postRepository.save(any())).willReturn(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword()));

        //when
        PostResponse postResponse = postService.write(postRequest, fixture.getUsername());

        //then
        assertDoesNotThrow(() -> postService.write(postRequest, fixture.getUsername()));
        assertEquals("포스트 등록 완료 ", postResponse.getMessage());
        assertEquals(1L, postResponse.getPostId());
    }

    @Test
    @DisplayName("게시물 등록 실패 - 유저 존재하지 않음")
    void post_fail() {
        //givne
        PostRequest postRequest = new PostRequest(fixture.getTitle(), fixture.getBody());
        given(userRepository.findByUsername(any())).willThrow(new AppException(ErrorCode.USERNAME_NOT_FOUND, ErrorCode.USERNAME_NOT_FOUND.getMessage()));

        //when
        AppException exception = assertThrows(AppException.class, () -> postService.write(postRequest, "sol"));

        //then
        assertEquals(exception.getErrorCode(), ErrorCode.USERNAME_NOT_FOUND);
        assertEquals(exception.getMessage(), ErrorCode.USERNAME_NOT_FOUND.getMessage());
    }

    /**
     * 포스트 상세 조회
     */
    @Test
    @DisplayName("게시물 한개 조회 - 성공")
    void get_one_post() {
        Long postId = fixture.getPostId();
        //Post를 찾는 로직을 가정
        Post mockPost = PostEntityFixture.get(fixture.getUsername(), fixture.getPassword());
        given(postRepository.findById(postId)).willReturn(Optional.of(mockPost));

        //when
        PostOneResponse result = postService.getPostById(postId);

        //then
        assertEquals(postId, result.getId());
        assertEquals(mockPost.getBody(), result.getBody());
        assertEquals(mockPost.getTitle(), result.getTitle());
        assertEquals(mockPost.getUser().getUsername(), result.getUsername());
    }

    @Test
    @DisplayName("한개 조회 실패 - 없는 게시글")
    void fail_get_one_post() {
        //Post 를 찾는 로직을 가정
        given(postRepository.findById(fixture.getPostId())).willReturn(Optional.empty());
        //when
        AppException e = assertThrows(AppException.class, () -> postService.getPostById(fixture.getPostId()));

        //then
        assertEquals(e.getErrorCode(), ErrorCode.POST_NOT_FOUND);
    }

    /**
     * 포스트 수정
     */
    @Test
    @DisplayName("게시글 수정 성공")
    void post_modify_success() {
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(postRepository.findById(fixture.getPostId())).willReturn(Optional.of(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));

        //when
        PostResponse result = assertDoesNotThrow(() -> postService.modify(fixture.getPostId(), postRequest, fixture.getUsername()));

        //then
        assertEquals(result.getMessage(), "포스트 수정 완료");
    }

    /**
     * 포스트 삭제 테스트
     */
    @Test
    @DisplayName("포스트 삭제 성공")
    void post_delete_success() {
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(postRepository.findById(fixture.getPostId())).willReturn(Optional.of(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        //when
        PostResponse result = postService.delete(fixture.getPostId(), fixture.getUsername());
        //then
        assertEquals(result.getMessage(), "포스트 삭제 완료");
    }
}