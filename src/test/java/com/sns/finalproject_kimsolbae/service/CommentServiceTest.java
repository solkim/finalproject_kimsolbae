package com.sns.finalproject_kimsolbae.service;

import com.sns.finalproject_kimsolbae.domain.dto.comment.CommentDeleteResponse;
import com.sns.finalproject_kimsolbae.domain.dto.comment.CommentRequest;
import com.sns.finalproject_kimsolbae.domain.dto.comment.CommentResponse;
import com.sns.finalproject_kimsolbae.domain.entity.Comment;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import com.sns.finalproject_kimsolbae.domain.entity.User;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.fixture.CommentEntityFixture;
import com.sns.finalproject_kimsolbae.fixture.PostEntityFixture;
import com.sns.finalproject_kimsolbae.fixture.TestInfoFixture;
import com.sns.finalproject_kimsolbae.fixture.UserEntityFixture;
import com.sns.finalproject_kimsolbae.repository.AlarmRepository;
import com.sns.finalproject_kimsolbae.repository.CommentRepository;
import com.sns.finalproject_kimsolbae.repository.PostRepository;
import com.sns.finalproject_kimsolbae.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@Slf4j
class CommentServiceTest {
    private final CommentRepository commentRepository = mock(CommentRepository.class);
    private final PostRepository postRepository = mock(PostRepository.class);
    private final UserRepository userRepository = mock(UserRepository.class);
    private final AlarmRepository alarmRepository = mock(AlarmRepository.class);
    private static CommentService commentService;
    private static TestInfoFixture.TestInfo fixture;
    private static User userA;
    private static Post postA;
    private static Comment commentA;
    private static Comment differentUserNameComment;
    private static CommentRequest commentRequest;

    @BeforeEach
    void setUp() {
        fixture = TestInfoFixture.get();
        userA = UserEntityFixture.get(fixture.getUsername(), fixture.getPassword());
        postA = PostEntityFixture.get(fixture.getUsername(), fixture.getPassword());
        commentA = CommentEntityFixture.get(userA, postA);
        differentUserNameComment = CommentEntityFixture.getDifferentUserComment("different", "댓글 내용 1");
        commentService = new CommentService(commentRepository, alarmRepository, userRepository, postRepository);
        commentRequest = new CommentRequest("댓글작성");
    }

    public static Stream<Arguments> provideErrorCases() {
        //beforeSetUp이 작동하지 않는다?
        User userA = UserEntityFixture.get("user a", "1");
        Post postA = PostEntityFixture.get("user a", "1");
        Comment commentA = CommentEntityFixture.get(userA,postA);
        Comment differentUserNameComment = CommentEntityFixture.getDifferentUserComment("different", "댓글 내용 1");

        return Stream.of(
                Arguments.of(Named.of("댓글 존재하지 않음", ErrorCode.COMMENT_NOT_FOUND), Optional.of(postA), Optional.of(userA), Optional.empty(), userA.getUsername()),
                Arguments.of(Named.of("게시글 존재하지 않음", ErrorCode.POST_NOT_FOUND), Optional.empty(), Optional.of(userA), Optional.of(commentA), userA.getUsername()),
                Arguments.of(Named.of("작성자 불일치", ErrorCode.ID_NOT_MATCH), Optional.of(postA), Optional.of(userA), Optional.of(differentUserNameComment), userA.getUsername())
        );
    }

    @ParameterizedTest
    @MethodSource("provideErrorCases")
    @DisplayName("수정, 삭제 실패")
    void fail_edit_comment(ErrorCode errorCode, Optional<Post> post, Optional<User> user, Optional<Comment> comment, String userName) {

        //given
        given(userRepository.findByUsername(any())).willReturn(user);
        given(postRepository.findById(any())).willReturn(post);
        given(commentRepository.findById(any())).willReturn(comment);

        //when
        AppException modifyError = assertThrows(AppException.class, () -> commentService.modify(commentRequest, fixture.getPostId(), fixture.getCommentId(), userName));
        AppException deleteError = assertThrows(AppException.class, () -> commentService.delete(fixture.getPostId(), fixture.getCommentId(), userName));

        //then
        assertEquals(modifyError.getErrorCode(), errorCode);
        assertEquals(modifyError.getMessage(), errorCode.getMessage());

        assertEquals(deleteError.getErrorCode(), errorCode);
        assertEquals(deleteError.getMessage(), errorCode.getMessage());
    }

    @Test
    @DisplayName("댓글 작성 성공")
    void comment_write_success() {
        //given
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getUsername())));
        given(postRepository.findById(any())).willReturn(Optional.ofNullable(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(commentRepository.save(any())).willReturn(commentA);

        //when
        CommentResponse write = commentService.write(fixture.getPostId(), fixture.getUsername(), commentRequest);

        //then
        assertDoesNotThrow(() -> commentService.write(fixture.getPostId(), fixture.getUsername(), commentRequest));
        assertEquals("comment 입니다.", write.getComment());
        assertEquals(commentA.getComment(), write.getComment());
        assertEquals(commentA.getId(), write.getId());
        assertEquals(commentA.getUser().getUsername(), write.getUserName());
    }

    @Test
    @DisplayName("댓글 작성 실패 - 게시물 없음")
    void comment_write_fail_게시물없음() {
        //given
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getUsername())));
        given(postRepository.findById(any())).willThrow(new AppException(
                ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()));
        given(commentRepository.save(any())).willReturn(commentA);
        //게시물이 없을경우

        //when
        AppException e = assertThrows(AppException.class, () -> commentService.write(fixture.getPostId(), fixture.getUsername(), commentRequest));

        //then
        assertEquals(e.getErrorCode(), ErrorCode.POST_NOT_FOUND);
        assertEquals(e.getMessage(), ErrorCode.POST_NOT_FOUND.getMessage());
    }

    @Test
    @DisplayName("댓글 수정 성공")
    void comment_modify_success() {
        //given
        given(userRepository.findByUsername(any())).willReturn(Optional.ofNullable(UserEntityFixture.get(fixture.getUsername(), fixture.getUsername())));
        given(postRepository.findById(any())).willReturn(Optional.ofNullable(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(commentRepository.findById(any())).willReturn(Optional.of(CommentEntityFixture.get(userA, postA)));

        //when
        CommentResponse modify = commentService.modify(commentRequest, fixture.getPostId(), fixture.getCommentId(), fixture.getUsername());

        //then
        assertDoesNotThrow(() -> commentService.modify(commentRequest, fixture.getPostId(), fixture.getCommentId(), fixture.getUsername()));
        assertEquals(modify.getComment(), commentRequest.getComment());
    }

    @Test
    @DisplayName("댓글 삭제 성공")
    void delete_success() {
        given(postRepository.findById(any())).willReturn(Optional.ofNullable(PostEntityFixture.get(fixture.getUsername(), fixture.getPassword())));
        given(commentRepository.findById(any())).willReturn(Optional.of(CommentEntityFixture.get(userA, postA)));

        //when
        CommentDeleteResponse result = commentService.delete(fixture.getPostId(), fixture.getCommentId(), fixture.getUsername());

        //then
        assertDoesNotThrow(() -> commentService.delete(fixture.getPostId(), fixture.getCommentId(), fixture.getUsername()));
        assertEquals(result.getMessage(), "댓글 삭제 완료");
        assertEquals(result.getId(), fixture.getPostId());
    }
}