package com.sns.finalproject_kimsolbae.fixture;

import com.sns.finalproject_kimsolbae.domain.entity.Like;
import com.sns.finalproject_kimsolbae.domain.entity.Post;

public class LikeEntityFixture {
    public static Like get(String userName, String password) {
        Like like = Like.builder()
                .id(1L)
                .user(UserEntityFixture.get(userName, password))
                .post(PostEntityFixture.get(userName, password))
                .build();
        return like;
    }
}
