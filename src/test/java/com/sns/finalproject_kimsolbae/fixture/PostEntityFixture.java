package com.sns.finalproject_kimsolbae.fixture;

import com.sns.finalproject_kimsolbae.domain.entity.Post;

public class PostEntityFixture {
    public static Post get(String userName, String password) {
        Post post = Post.builder()
                .id(1L)
                .user(UserEntityFixture.get(userName,password))
                .title("title")
                .body("body")
                .build();
        return post;
    }
}
