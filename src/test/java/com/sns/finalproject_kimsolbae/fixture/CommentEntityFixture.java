package com.sns.finalproject_kimsolbae.fixture;

import com.sns.finalproject_kimsolbae.domain.entity.Comment;
import com.sns.finalproject_kimsolbae.domain.entity.Post;
import com.sns.finalproject_kimsolbae.domain.entity.User;

public class CommentEntityFixture {
    public static Comment get(User user, Post post) {
        Comment comment = Comment.builder()
                .id(1L)
                .user(user)
                .post(post)
                .comment("comment 입니다.")
                .build();
        return comment;
    }

    public static Comment getDifferentUserComment(String userName, String password) {
        Comment comment = Comment.builder()
                .id(1L)
                .user(UserEntityFixture.get(userName,password))
                .post(PostEntityFixture.get(userName,password))
                .comment("comment 입니다.")
                .build();
        return comment;
    }
}
