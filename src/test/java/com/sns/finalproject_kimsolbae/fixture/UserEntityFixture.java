package com.sns.finalproject_kimsolbae.fixture;

import com.sns.finalproject_kimsolbae.domain.UserRole;
import com.sns.finalproject_kimsolbae.domain.entity.User;

public class UserEntityFixture {
    public static User get(String username, String password) {
        return User.builder()
                .id(1L)
                .username(username)
                .password(password)
                .role(UserRole.ROLE_USER)
                .build();
    }
    public static User getADMIN(String username, String password){
        return User.builder()
                .id(1L)
                .username(username)
                .password(password)
                .role(UserRole.ROLE_ADMIN)
                .build();
    }
}
