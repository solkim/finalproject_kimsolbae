package com.sns.finalproject_kimsolbae.fixture;

import com.sns.finalproject_kimsolbae.domain.AlarmType;
import com.sns.finalproject_kimsolbae.domain.entity.Alarm;
import com.sns.finalproject_kimsolbae.domain.entity.Post;

public class AlarmEntityFixture {
    public static Alarm get(String userName, String password) {
        Alarm alarm = Alarm.builder()
                .id(1L)
                .user(UserEntityFixture.get(userName, password))
                .text("성공")
                .targetId(1L)
                .fromUserId(1L)
                .alarmType(AlarmType.NEW_COMMENT_ON_POST)
                .build();
        return alarm;
    }
}
