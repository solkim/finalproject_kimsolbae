package com.sns.finalproject_kimsolbae.enums;

import lombok.Getter;

@Getter
public enum CommentTestEnum {

    COMMENT_URL("/api/v1/posts/1/comments/1");

    private final String value;
    CommentTestEnum(String value) {
        this.value = value;
    }
}

