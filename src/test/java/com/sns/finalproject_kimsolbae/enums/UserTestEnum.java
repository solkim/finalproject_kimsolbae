package com.sns.finalproject_kimsolbae.enums;

import lombok.Getter;

@Getter
public enum UserTestEnum {

    USER_URL("/api/v1/users/login");
    private final String value;
    UserTestEnum(String value) {
        this.value = value;
    }
}

