package com.sns.finalproject_kimsolbae.enums;

import lombok.Getter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
public enum PostTestEnum {

    POST_URL("/api/v1/posts/1"),
    POST_CREATE_MESSAGE("포스트 등록 완료"),
    POST_EDIT_MESSAGE("포스트 수정 완료"),
    POST_DELETE_MESSAGE("포스트 삭제 완료");

    private final String value;
    PostTestEnum(String value) {
        this.value = value;
    }
}

