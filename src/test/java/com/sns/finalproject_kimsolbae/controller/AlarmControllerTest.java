package com.sns.finalproject_kimsolbae.controller;

import com.sns.finalproject_kimsolbae.controller.restcontroller.AlarmController;
import com.sns.finalproject_kimsolbae.domain.dto.alarm.AlarmResponse;
import com.sns.finalproject_kimsolbae.fixture.AlarmEntityFixture;
import com.sns.finalproject_kimsolbae.service.AlarmService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AlarmController.class)
class AlarmControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AlarmService alarmService;

    @Test
    @WithMockUser
    @DisplayName("알람 조회 성공")
    void success_getAlarms() throws Exception {
        //given
        List<AlarmResponse> list = Arrays.asList(
                AlarmResponse.fromEntity(AlarmEntityFixture.get("userName", "password")),
                AlarmResponse.fromEntity(AlarmEntityFixture.get("userName1", "password1")),
                AlarmResponse.fromEntity(AlarmEntityFixture.get("userName2", "password2")),
                AlarmResponse.fromEntity(AlarmEntityFixture.get("userName3", "password3"))
        );
        Page<AlarmResponse> response = new PageImpl<>(list);
        given(alarmService.getAlarms(any(), any())).willReturn(response);

        //when
        mockMvc.perform(get("/api/v1/alarms").with(
                csrf()))
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$['result']['content'][0]['id']").value(1L))
                .andExpect(jsonPath("$['result']['content'].size()").value(4));
    }

    @Test
    @WithAnonymousUser
    @DisplayName("알람 조회 실패")
    void fail_getAlarms() throws Exception {
        //given
        List<AlarmResponse> list = Arrays.asList(
                AlarmResponse.fromEntity(AlarmEntityFixture.get("userName", "password")),
                AlarmResponse.fromEntity(AlarmEntityFixture.get("userName1", "password1")),
                AlarmResponse.fromEntity(AlarmEntityFixture.get("userName2", "password2")),
                AlarmResponse.fromEntity(AlarmEntityFixture.get("userName3", "password3"))
        );
        Page<AlarmResponse> response = new PageImpl<>(list);
        given(alarmService.getAlarms(any(), any())).willReturn(response);

        //when
        mockMvc.perform(get("/api/v1/alarms").with(
                        csrf()))
                //then
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}