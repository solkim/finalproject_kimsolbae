package com.sns.finalproject_kimsolbae.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.controller.restcontroller.LikeController;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.service.LikeService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LikeController.class)
class LikeControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    LikeService likeService;

    @Autowired
    ObjectMapper objectMapper;

    /**
     *
     *  좋아요 기능
     *
     */
    @Test
    @WithMockUser
    @DisplayName("좋아요 성공")
    void Like_success() throws Exception {

        //given
        String result = "좋아요를 눌렀습니다.";
        given(likeService.like(any(),any())).willReturn(result);

        //when
        mockMvc.perform(post("/api/v1/posts/1/likes")
                .with(csrf())
        )
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result").exists())
        ;
    }
    @Test
    @WithAnonymousUser
    @DisplayName("좋아요 실패 - 인증실패")
    void Like_fail_인증실패() throws Exception {

        //given
        given(likeService.like(any(), any())).willThrow(new AppException(
                ErrorCode.INVALID_PERMISSION, ErrorCode.INVALID_PERMISSION.getMessage()
        ));

        //when
        mockMvc.perform(post("/api/v1/posts/1/likes")
                        .with(csrf())
                )
                //then
                .andDo(print())
                .andExpect(status().isUnauthorized())
        ;
    }
    @Test
    @WithMockUser
    @DisplayName("좋아요 실패 - post가 없음")
    void Like_fail_post없음() throws Exception {

        //given
        given(likeService.like(any(), any())).willThrow(new AppException(
                ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()
        ));

        //when
        mockMvc.perform(post("/api/v1/posts/1/likes")
                        .with(csrf())
                )
                //then
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.message").value("해당 포스트가 없습니다."))
                .andExpect(jsonPath("$.result.errorCode").value("POST_NOT_FOUND"))

        ;
    }

    /**
     * 좋아요 조회
     */
    @Test
    @WithMockUser
    @DisplayName("좋아요 조회 - 성공")
    void like_get_success() throws Exception {

        given(likeService.getLikes(any())).willReturn(1L);

        mockMvc.perform(get("/api/v1/posts/1/likes")
                )
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result").value(1L));
    }
    @Test
    @WithMockUser
    @DisplayName("좋아요 조회 실패 - post없음")
    void like_get_fail() throws Exception {

        given(likeService.getLikes(any())).willThrow(new AppException(
                ErrorCode.POST_NOT_FOUND, ErrorCode.POST_NOT_FOUND.getMessage()
        ));

        mockMvc.perform(get("/api/v1/posts/1/likes")
                )
                .andExpect(status().isNotFound())
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.message").value("해당 포스트가 없습니다."))
                .andExpect(jsonPath("$.result.errorCode").value("POST_NOT_FOUND"));
        ;
    }
}