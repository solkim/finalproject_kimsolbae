package com.sns.finalproject_kimsolbae.controller;

import com.sns.finalproject_kimsolbae.controller.restcontroller.HelloRestController;
import com.sns.finalproject_kimsolbae.service.GetSumOfDigit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloRestController.class)
class HelloControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    GetSumOfDigit getSumOfDigit;



    @Test
    @WithMockUser
    @DisplayName("hello 이름이 잘 나오는지 테스트")
    void hello() throws Exception {

        mockMvc.perform(get("/api/v1/hello")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("김솔배"));
    }

    @Test
    @WithMockUser
    @DisplayName("자리수 합이 잘 나오는지 테스트")
    void sumOfDigit() throws Exception {

        when(getSumOfDigit.sumOfDigit(any())).thenReturn(10L);

        mockMvc.perform(get("/api/v1/hello/1234")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("10"));
    }
}

