package com.sns.finalproject_kimsolbae.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.controller.restcontroller.PostRestController;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostRequest;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostResponse;
import com.sns.finalproject_kimsolbae.enums.PostTestEnum;
import com.sns.finalproject_kimsolbae.enums.UserTestEnum;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.fixture.PostEntityFixture;
import com.sns.finalproject_kimsolbae.service.PostService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;


import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PostRestController.class)
class PostRestControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    PostService postService;

    Long postId = 1L;
    PostRequest postRequest = new PostRequest("title", "body");
    PostResponse postResponse = new PostResponse("success", postId);
    PostOneResponse postOneResponse = new PostOneResponse(
            1L, " ", " ", " ", LocalDateTime.now() , LocalDateTime.now());

    private static Stream<Arguments> provideErrorCases() {
        return Stream.of(
                Arguments.of(Named.of("인증 실패", status().isUnauthorized()), ErrorCode.INVALID_PERMISSION),
                Arguments.of(Named.of("작성자 불일치", status().isUnauthorized()), ErrorCode.INVALID_PERMISSION),
                Arguments.of(Named.of("DB 에러", status().isInternalServerError()), ErrorCode.DATABASE_ERROR));
    }



    //---------------------------------------조회 테스트-----------------------------------
    @Test
    @DisplayName("게시글 조회 성공")
    @WithMockUser
    void post_get_success() throws Exception {

        given(postService.getPostById(any())).willReturn(postOneResponse);

        mockMvc.perform(get("/api/v1/posts/1")
                                .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.id").value(1L))
//                .andExpect(jsonPath("$.result.id").value(2L))
                .andExpect(jsonPath("$.result.title").exists())
                .andExpect(jsonPath("$.result.body").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.createdAt").exists())
                .andExpect(jsonPath("$.result.lastModifiedAt").exists())
        ;

        verify(postService).getPostById(any());
    }
    //---------------------------------------등록 테스트-----------------------------------

    @Test
    @WithMockUser
    @DisplayName("게시물 등록 성공")
    void post_write_success() throws Exception {

        when(postService.write(any(), any())).thenReturn(postResponse);

        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.postId").value(1L))
                .andExpect(jsonPath("$.result.message").value(postResponse.getMessage()))
        ;
        verify(postService).write(any(), any());
    }

    @Test
    @WithAnonymousUser
    @DisplayName("게시물 등록 실패 - 인증실패")
    void post_write_fail() throws Exception {

        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
        ;
    }

    //---------------------------------------수정 테스트-----------------------------------
    @Test
    @DisplayName("게시글 수정 성공")
    @WithMockUser
    void post_modify_success() throws Exception {

        given(postService.modify(any(),any(),any())).willReturn(postResponse);

        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postResponse))
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.postId").value(postId))
                .andExpect(jsonPath("$.result.message").exists())
        ;
        verify(postService).modify(any(), any(), any());
    }
    @ParameterizedTest
    @DisplayName("게시글 수정 실패")
    @WithMockUser
    @MethodSource("provideErrorCases")
    void fail_post_modify(ResultMatcher error, ErrorCode code) throws Exception {
        given(postService.modify(any(), any(), any())).willThrow(new AppException(code, code.getMessage()));

        mockMvc.perform(put(PostTestEnum.POST_URL.getValue())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postResponse)).with(csrf()))
                .andExpect(error)
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value(code.name()))
                .andExpect(jsonPath("$.result.message").value(code.getMessage()))
                .andDo(print());

        verify(postService).modify(any(),any(),any());
    }
    //---------------------------------------삭제 테스트-----------------------------------
    @Test
    @WithMockUser
    @DisplayName("게시물 삭제 성공")
    void post_delete_success() throws Exception {

        given(postService.delete(any(),any())).willReturn(postResponse);

        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postId))
                )
                .andDo(print())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.message").value("success"))
                .andExpect(jsonPath("$.result.postId").value(1L))
                .andExpect(status().isOk())
        ;
        verify(postService).delete(any(), any());
    }
    @ParameterizedTest
    @DisplayName("게시글 삭제 실패")
    @WithMockUser
    @MethodSource("provideErrorCases")
    void fail_post_delete(ResultMatcher error, ErrorCode code) throws Exception {
        given(postService.delete(any(), any())).willThrow(new AppException(code, code.getMessage()));

        mockMvc.perform(delete(PostTestEnum.POST_URL.getValue())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postResponse)).with(csrf()))
                .andExpect(error)
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value(code.name()))
                .andExpect(jsonPath("$.result.message").value(code.getMessage()))
                .andDo(print());

        verify(postService).delete(any(),any());
    }
    //---------------------------------------list 조회 테스트-----------------------------------
    @Test
    @WithMockUser
    @DisplayName("게시물 리스트 조회")
    void post_getList_success() throws Exception {

        List<PostOneResponse> list = List.of(PostOneResponse.fromEntity(
                PostEntityFixture.get("userName123", "password")
        ));
        Page<PostOneResponse> responses = new PageImpl<>(list);

        given(postService.getPostsList(any())).willReturn(responses);

        mockMvc.perform(get("/api/v1/posts")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$['result']['content'][0]['userName']").value("userName123"));

    }
    @Test
    @WithMockUser
    @DisplayName("게시물 리스트 조회2")
    void post_getList_success2() throws Exception {

        mockMvc.perform(get("/api/v1/posts")
                        .with(csrf())
                        .param("page", "0")
                        .param("size", "20")
                        .param("sort", "id")
                        .param("direction", "Sort.Direction.DESC"))
                .andExpect(status().isOk())
        ;
        // 메소드가 받을 인자
        ArgumentCaptor<Pageable> pageableArgumentCaptor = ArgumentCaptor.forClass(Pageable.class);
        //postService의 getPostsList메서드의 인자들을 pageableArgumentCaptor에 받아온다.
        verify(postService).getPostsList(pageableArgumentCaptor.capture());
        //getValue를 통해 메서드의 인자를 값으로 할당한다.
        PageRequest pageRequest = (PageRequest) pageableArgumentCaptor.getValue();

        assertEquals(20, pageRequest.getPageSize());
        assertEquals(Sort.by("id", "DESC"), pageRequest.withSort(Sort.by("id", "DESC")).getSort());
    }
}