package com.sns.finalproject_kimsolbae.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.controller.restcontroller.CommentController;
import com.sns.finalproject_kimsolbae.domain.dto.comment.CommentDeleteResponse;
import com.sns.finalproject_kimsolbae.domain.dto.comment.CommentRequest;
import com.sns.finalproject_kimsolbae.domain.dto.comment.CommentResponse;
import com.sns.finalproject_kimsolbae.domain.dto.post.PostOneResponse;
import com.sns.finalproject_kimsolbae.enums.CommentTestEnum;
import com.sns.finalproject_kimsolbae.enums.PostTestEnum;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.fixture.CommentEntityFixture;
import com.sns.finalproject_kimsolbae.fixture.PostEntityFixture;
import com.sns.finalproject_kimsolbae.fixture.TestInfoFixture;
import com.sns.finalproject_kimsolbae.fixture.UserEntityFixture;
import com.sns.finalproject_kimsolbae.service.CommentService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CommentController.class)
class CommentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    CommentService commentService;

    private TestInfoFixture.TestInfo fixture = TestInfoFixture.get();

    private CommentRequest commentRequest = new CommentRequest("댓글작성");
    private CommentResponse commentResponse = new CommentResponse(
            fixture.getUserId(),fixture.getBody(),fixture.getUsername(),fixture.getPostId(), LocalDateTime.now(),LocalDateTime.now()
    );

    private static Stream<Arguments> provideErrorCases() {
        return Stream.of(
                Arguments.of(Named.of("인증 실패", status().isUnauthorized()), ErrorCode.INVALID_PERMISSION),
                Arguments.of(Named.of("Post 없음", status().isNotFound()), ErrorCode.POST_NOT_FOUND),
                Arguments.of(Named.of("작성자 불일치", status().isUnauthorized()), ErrorCode.INVALID_PERMISSION),
                Arguments.of(Named.of("DB 에러", status().isInternalServerError()), ErrorCode.DATABASE_ERROR));
    }




    @Test
    @DisplayName("댓글 작성 성공")
    @WithMockUser
    void Comment_success() throws Exception {

        given(commentService.write(any(),any(),any())).willReturn(commentResponse);

        //when
        mockMvc.perform(post("/api/v1/posts/1/comments")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(commentRequest))
        )
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").exists())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.comment").value(fixture.getBody()))
                .andExpect(jsonPath("$.result.userName").value(fixture.getUsername()))
                .andExpect(jsonPath("$.result.postId").value(fixture.getPostId()))
        ;
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 수정 성공")
    void Comment_modify_success() throws Exception {

        given(commentService.modify(any(), any(), any(),any())).willReturn(commentResponse);

        //when
        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentRequest))
                )
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.comment").value(commentResponse.getComment()))
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.comment").value(fixture.getBody()))
                .andExpect(jsonPath("$.result.userName").value(fixture.getUsername()))
                .andExpect(jsonPath("$.result.postId").value(fixture.getPostId()))
        ;
    }
    @ParameterizedTest
    @DisplayName("댓글 수정 실패")
    @WithMockUser
    @MethodSource("provideErrorCases")
    void fail_comment_modify(ResultMatcher error, ErrorCode code) throws Exception {
        given(commentService.modify(any(),any(), any(), any())).willThrow(new AppException(code, code.getMessage()));

        mockMvc.perform(put(CommentTestEnum.COMMENT_URL.getValue())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentResponse)).with(csrf()))
                .andExpect(error)
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value(code.name()))
                .andExpect(jsonPath("$.result.message").value(code.getMessage()))
                .andDo(print());

        verify(commentService).modify(any(),any(),any(),any());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 삭제 성공")
    void delete_success() throws Exception {

        //given
        CommentDeleteResponse deleteDto = CommentDeleteResponse.of(1L);
        given(commentService.delete(any(), any(), any())).willReturn(deleteDto);

        //when
        //넘겨줄 Body가 없다.
        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                .with(csrf())
        )
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$.result.message").value("댓글 삭제 완료"))
                .andExpect(jsonPath("$.result.id").value(1L))
        ;
        verify(commentService).delete(any(),any(),any());
    }

    @ParameterizedTest
    @DisplayName("댓글 삭제 실패")
    @WithMockUser
    @MethodSource("provideErrorCases")
    void fail_comment_delete(ResultMatcher error, ErrorCode code) throws Exception {
        given(commentService.delete(any(),any(), any())).willThrow(new AppException(code, code.getMessage()));

        mockMvc.perform(delete(CommentTestEnum.COMMENT_URL.getValue())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentResponse)).with(csrf()))
                .andExpect(error)
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value(code.name()))
                .andExpect(jsonPath("$.result.message").value(code.getMessage()))
                .andDo(print());

        verify(commentService).delete(any(),any(),any());
    }

    @Test
    @WithMockUser
    @DisplayName("댓글 리스트 조회")
    void post_getList_success() throws Exception {

        List<CommentResponse> list = List.of(CommentResponse.fromEntity(
                CommentEntityFixture.get(UserEntityFixture.get("user a", "password"), PostEntityFixture.get("user a", "password"))
        ),
        CommentResponse.fromEntity(
                CommentEntityFixture.get(UserEntityFixture.get("user b", "password"), PostEntityFixture.get("user b", "password"))
        )
        );
        Page<CommentResponse> responses = new PageImpl<>(list);

        given(commentService.getCommentList(any(), any())).willReturn(responses);

        mockMvc.perform(get("/api/v1/posts/1/comments")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.resultCode").value("SUCCESS"))
                .andExpect(jsonPath("$['result']['content'][0]['userName']").value("user a"))
                .andExpect(jsonPath("$['result']['content'][1]['userName']").value("user b"))
                .andExpect(jsonPath("$['result']['content'].size()").value(2))
        ;

    }
}