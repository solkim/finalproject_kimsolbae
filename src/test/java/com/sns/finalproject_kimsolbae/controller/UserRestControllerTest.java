package com.sns.finalproject_kimsolbae.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.controller.restcontroller.UserRestController;
import com.sns.finalproject_kimsolbae.domain.dto.token.TokenResponseDto;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinRequest;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinResponse;
import com.sns.finalproject_kimsolbae.domain.dto.user.UserLoginRequest;
import com.sns.finalproject_kimsolbae.enums.PostTestEnum;
import com.sns.finalproject_kimsolbae.enums.UserTestEnum;
import com.sns.finalproject_kimsolbae.exception.AppException;
import com.sns.finalproject_kimsolbae.exception.ErrorCode;
import com.sns.finalproject_kimsolbae.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.stream.Stream;

import static com.sns.finalproject_kimsolbae.exception.ErrorCode.DUPLICATED_USER_NAME;
import static com.sns.finalproject_kimsolbae.exception.ErrorCode.USERNAME_NOT_FOUND;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserRestController.class)
class UserRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;
    
    @MockBean
    UserService userService;

    UserJoinRequest userJoinRequest = new UserJoinRequest("id","password");
    UserLoginRequest userLoginRequest = new UserLoginRequest("1","2");
    private static Stream<Arguments> provideErrorCases() {
        return Stream.of(
                Arguments.of(Named.of("아이디가 없음", status().isNotFound()), ErrorCode.USERNAME_NOT_FOUND),
                Arguments.of(Named.of("비밀번호가 잘못됨", status().isUnauthorized()), ErrorCode.INVALID_PASSWORD));
    }



    @Test
    @DisplayName("회원가입 성공")
    @WithMockUser
    void join_success() throws Exception {

        //given
        given(userService.join(any())).willReturn(new UserJoinResponse(0L,"id"));

        //when
        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userJoinRequest)))

                //then

                .andDo(print())
                .andExpect(jsonPath("$.result.userId").value(0L))
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(status().isOk())
        ;
    }

    @Test
    @DisplayName("회원가입 실패 - 아이디 중복")
    @WithMockUser
    void join_fail() throws Exception {

        given(userService.join(any())).willThrow(new AppException(DUPLICATED_USER_NAME, "해당 user가 중복됩니다."));

        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userJoinRequest)))
                .andDo(print())
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.result.errorCode").value("DUPLICATED_USER_NAME"))
                .andExpect(jsonPath("$.result.message").value("해당 user가 중복됩니다."))
        ;
    }

    @Test
    @DisplayName("로그인 성공")
    @WithMockUser
    void login_success() throws Exception {
        TokenResponseDto tokenRequestDto = new TokenResponseDto("token", "refreshToken");

        given(userService.login(any())).willReturn(tokenRequestDto);


        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userLoginRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.accessToken").value("token"))
                .andExpect(jsonPath("$.result.refreshToken").value("refreshToken"))
        ;

        verify(userService).login(any());
    }
    @ParameterizedTest
    @DisplayName("실패")
    @WithMockUser
    @MethodSource("provideErrorCases")
    void fail_login(ResultMatcher error, ErrorCode code) throws Exception {
        given(userService.login(any())).willThrow(new AppException(code, code.getMessage()));

        mockMvc.perform(post(UserTestEnum.USER_URL.getValue()).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(userJoinRequest)).with(csrf()))
                .andExpect(error)
                .andExpect(jsonPath("$.resultCode").value("ERROR"))
                .andExpect(jsonPath("$.result.errorCode").value(code.name()))
                .andExpect(jsonPath("$.result.message").value(code.getMessage()))
                .andDo(print());

        verify(userService).login(any());
    }
}