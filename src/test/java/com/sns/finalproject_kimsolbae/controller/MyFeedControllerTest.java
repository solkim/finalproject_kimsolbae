package com.sns.finalproject_kimsolbae.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sns.finalproject_kimsolbae.controller.restcontroller.MyFeedRestController;
import com.sns.finalproject_kimsolbae.domain.dto.myFeed.MyFeedResponse;
import com.sns.finalproject_kimsolbae.fixture.PostEntityFixture;
import com.sns.finalproject_kimsolbae.service.MyFeedService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MyFeedRestController.class)
class MyFeedControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    MyFeedService myFeedService;

    @Test
    @WithMockUser
    @DisplayName("마이피드 조회 성공")
    void get_myFeeds_success() throws Exception {
        //given
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, "id");
        List<MyFeedResponse> list = List.of(MyFeedResponse.fromEntity(PostEntityFixture.get(
                "userName", "password"
        )));
        //List -> Page
        Page<MyFeedResponse> response = new PageImpl<>(list);

        given(myFeedService.getMyFeeds(any(),any())).willReturn(response);

        //안되는 이유는 mockMvc에서 Controller에 Body값을 안넘겨줬기 때문인가?
//        given(myFeedService.getMyFeeds(pageable,"userName")).willReturn(response);

        //when
        mockMvc.perform(get("/api/v1/posts/my")
                        .with(csrf()))
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.content").exists())
                .andExpect(jsonPath("$.result.pageable").exists())
                .andExpect(jsonPath("$['result']['content'][0]['userName']").value("userName"))
                .andExpect(jsonPath("$['result']['content'][0]['body']").exists())
                .andExpect(jsonPath("$['result']['content'].size()").value(1));

    }
    @Test
    @WithMockUser
    @DisplayName("마이피드 조회 성공1")
    void get_myFeeds_success1() throws Exception {
        //given
        List<MyFeedResponse> list = Arrays.asList(
                MyFeedResponse.fromEntity(PostEntityFixture.get("userName", "password")),
                MyFeedResponse.fromEntity(PostEntityFixture.get("userName1", "password1")),
                MyFeedResponse.fromEntity(PostEntityFixture.get("userName2", "password2")),
                MyFeedResponse.fromEntity(PostEntityFixture.get("userName3", "password3"))
        );
        Page<MyFeedResponse> response = new PageImpl<>(list);
        given(myFeedService.getMyFeeds(any(), any())).willReturn(response);

        //when
        given(myFeedService.getMyFeeds(any(),any())).willReturn(response);
        mockMvc.perform(get("/api/v1/posts/my")
                        .with(csrf()))
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.content").exists())
                .andExpect(jsonPath("$.result.pageable").exists())
                .andExpect(jsonPath("$['result']['content'][0]['userName']").value("userName"))
                .andExpect(jsonPath("$['result']['content'][0]['body']").exists())
                .andExpect(jsonPath("$['result']['content'].size()").value(4));

    }
    @Test
    @DisplayName("마이피드 조회 실패 - 인증실패")
    @WithAnonymousUser
    void show_my_list_fail() throws Exception {
        mockMvc.perform(
                        get("/api/v1/posts/my")
                                .with(csrf()))
                .andExpect(status().isUnauthorized())
                .andDo(print());
    }
}
