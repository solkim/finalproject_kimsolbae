//package com.sns.finalproject_kimsolbae.repository;
//
//import com.sns.finalproject_kimsolbae.domain.UserRole;
//import com.sns.finalproject_kimsolbae.domain.dto.user.UserJoinRequest;
//import com.sns.finalproject_kimsolbae.domain.entity.Post;
//import com.sns.finalproject_kimsolbae.domain.entity.User;
//import com.sns.finalproject_kimsolbae.service.PostService;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import java.io.FilterOutputStream;
//import java.time.LocalDateTime;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@DataJpaTest
//class PostRepositoryTest {
//
//    @Autowired
//    PostRepository postRepository;
//    @Autowired
//    UserRepository userRepository;
//
//    @Test
//    @DisplayName("soft delete 테스트")
//    void post_softDelete_success() {
//        //Post에 적용할 user를 생성
//        User user = User.builder()
//                .userName("userName")
//                .password("password")
//                .role(UserRole.ROLE_USER)
//                .build();
//        //user 저장
//        User saveUser = userRepository.save(user);
//        //Post 생성
//        Post post = Post.builder()
//                .title("test")
//                .body("softDeleteTest")
//                .user(saveUser)
//                .build();
//        //글쓰기
//        Post savePost = postRepository.save(post);
//
//        //게시글 확인
//        assertEquals(postRepository.count(),1L);
//        assertEquals(savePost.isSoftDeleted(),false);
//
//        //게시글 삭제
//        savePost.deleteSoftly(LocalDateTime.now());
//
//        //게시글 삭제 확인
//        assertEquals(postRepository.count(),0L);
//        assertEquals(savePost.isSoftDeleted(),true);
//    }
//}