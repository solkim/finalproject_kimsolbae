//package com.sns.finalproject_kimsolbae.repository;
//
//import com.sns.finalproject_kimsolbae.domain.entity.RedisToken;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@SpringBootTest
//class RedisTokenRepositoryTest {
//    @Autowired
//    private RedisTokenRepository repo;
//
//    @BeforeEach
//    void setUp() {
//    }
//
//    @Test
//    void test() {
//        RedisToken redisToken = new RedisToken(1L,"accessToken", "refreshToken");
//
//        // 저장
//        repo.save(redisToken);
//
//        // `keyspace:id` 값을 가져옴
//        repo.findById(redisToken.getId());
//
//        // Person Entity 의 @RedisHash 에 정의되어 있는 keyspace (people) 에 속한 키의 갯수를 구함
//        repo.count();
//
//        // 삭제
//        repo.delete(redisToken);
//    }
//
//}